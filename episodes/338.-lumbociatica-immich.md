---
number: 339
identifier: 338.-lumbociatica-immich
title: Lumbociatica e immich
subject:
- Lumbociática
- ciática
- dolor
- molestias
- immich
- Papá Friki
downloads: 775
filename: 338.LumbociaticaImmich.mp3
datetime: 2024-05-23T11:42:46Z
version: 1
size: 7957845
length: 500
excerpt: Buenas muchachada hoy os cuento sobre mis problemas con una lumbociatica y mi uso del gestor de fotos immich
slug: lumbociatica-e-immich
---
Buenas muchachada hoy os cuento sobre mis molestias con una lumbociática y el programa immich para gestionar las fotos localmente.

CC-Enfermera

[https://podcasters.spotify.com/pod/show/alberto5757/episodes/CC-Enfermera-e2j5e3r](https://podcasters.spotify.com/pod/show/alberto5757/episodes/CC-Enfermera-e2j5e3r)