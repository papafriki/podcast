---
layout: page
title: Acerca de
permalink: /about/
---

Mi nombre es Alberto (algo más conocido como Papá Friki) y aquí podrás encontrar batallitas del día a día de un Papá Friki.

El proyecto se inció en marzo de 2018, desde entonces he compartido mis vivencias, mis proyectos y he disfrutado de los mas de 200 episodios como si fueran el primero.


Este podcast está asociado a la red de SOSPECHOSOS HABITUALES, puedes suscribirte a la red con este feed:

https://feedpress.me/sospechososhabituales


Gracias por estar ahí. Un saludo nos vemos, nos leemos nos escuchamos... adiós!!!
