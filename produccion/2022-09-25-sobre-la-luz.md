---
layout: post  
title: "PPF-Sobre la luz"  
date: 2022-09-25  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/252.-sobre-la-luz/252.SobreLaLuz  
tags: [Movistar, Digi, Vodafone, Portabilidad, Wondergy, Tope de gas, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre cambio de compañía de la luz y del teléfono de casa.  

<br><br>
Perdonar el audio que ha quedado con mucho eco, es lo que tiene no tener un sitio fijo aún para grabar.
<br><br>
Podcast de la semana: Disperso -- Un viaje en autobús #reflexión
<br><br>
[https://www.ivoox.com/un-viaje-autobus-reflexion-audios-mp3_rf_92782913_1.html](https://www.ivoox.com/un-viaje-autobus-reflexion-audios-mp3_rf_92782913_1.html)
<br><br>
He contratado la luz mediante
<br><br>
[https://wondergy.es/](https://wondergy.es/)

<br>
<audio controls>
  <source src="https://archive.org/download/252.-sobre-la-luz/252.SobreLaLuz.mp3" type="audio/mpeg">  
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>

