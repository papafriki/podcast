---
layout: post  
title: "PPF-Como grabo el podcast"  
date: 2020-11-05  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/151.-como-grabo-el-podcast/151.ComoGraboElPodcast  
tags: [Photopea, Audacity, Gitlab, Archive.org, Twitter, Youtube, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os explico como es el proceso de grabación de un podcast de Papá Friki, el proceso que sigo y los programas que uso.  


<audio controls>
  <source src="https://archive.org/download/151.-como-grabo-el-podcast/151.ComoGraboElPodcast.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
