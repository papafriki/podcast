---
layout: post  
title: "PPF-Bienvenido Mr Antonio, variado y menores en noticias"  
date: 2021-06-16  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/186.-tecnocincuentones-netflix-menores/186.TecnocincuentonesNetflixMenores  
tags: [Sospechos Habituales, Tecnocincuentones, Antonio Manfredi, Netflix, Lenovo, T430, Papá Friki]  
comments: true 
---
Buenas muchachada hoy damos la bienvenida a la red de sospechosos habituales a Antonio Manfredi con su Tecnocincuentones, variado de compras y Netflix turco para terminar con una reflexión sobre los menores en las noticias.  

Dejo el link de compra de las tarjetas en liras turcas. Y recuerdo que hay que conectar con una VPN a Turquía para hacer el canje del cupón adquirido.

[https://www.g2a.com/search?query=netflix](https://www.g2a.com/search?query=netflix)


<audio controls>
  <source src="https://archive.org/download/186.-tecnocincuentones-netflix-menores/186.TecnocincuentonesNetflixMenores.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
