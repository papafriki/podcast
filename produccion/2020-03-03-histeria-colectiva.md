---
layout: post  
title: "Histeria colectiva"  
date: 2020-03-03  
categories: podcast
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/119.histeriacolectiva/119.HisteriaColectiva  
tags: [COVID-19, Surface, Valencia, SMB, Papá Friki]  
comments: true 
---
Buenas muchachada, os hablo de la llegada del coronavirus a Torrejón de Ardoz y más experiencia de uso con la Surface Pro 4.  


Os dejo por aquí lo que activé en windows por si a alguien le sirve en un futuro.


![](https://i.imgur.com/AN7fBdb.png)



<audio controls>
  <source src="https://archive.org/download/119.histeriacolectiva/119.HisteriaColectiva.mp3" type="audio/mpeg">
</audio>




