---
layout: post  
title: "IOS omv y sobre el MacBook pro"  
date: 2019-05-12  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/077.IOSTelecomunicacionesMac/077.IOSTelecomunicacionesMac  
tags: [ MacBook Pro, IOS omv, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os comento que tal el uso de la omv IOS y mas sobre mi uso del macbook pro.  


<audio controls>
  <source src="https://archive.org/download/077.IOSTelecomunicacionesMac/077.IOSTelecomunicacionesMac.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
