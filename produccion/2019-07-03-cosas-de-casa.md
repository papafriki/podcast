---
layout: post  
title: "Cosas de casa"  
date: 2019-07-03  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/083.cosasdecasa/083.Cosasdecasa  
tags: [ Leire, Dislexia, Nuria, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento un par de temas familiares.  


<audio controls>
  <source src="https://archive.org/download/083.cosasdecasa/083.Cosasdecasa.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
