---
layout: post  
title: "DNI electrónico y consejos al denunciar"  
date: 2019-05-26  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/079.DniElectronico/079.DniElectronico  
tags: [ DNIe, Consejos, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os comento aplicaciones que podéis usar con el D.N.I. electrónico y el chip nfc en moviles o tabletas con android.  

El instalador que comento y es necesario para el pc que tenga widows instalado es el siguiente:

[https://www.dnielectronico.es/descargas/CSP_para_Sistemas_Windows/Windows_64_bits/DNIe_v14_0_2(64bits).exe](https://www.dnielectronico.es/descargas/CSP_para_Sistemas_Windows/Windows_64_bits/DNIe_v14_0_2(64bits).exe)

<audio controls>
  <source src="https://archive.org/download/079.DniElectronico/079.DniElectronico.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
