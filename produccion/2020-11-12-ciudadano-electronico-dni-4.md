---
layout: post  
title: "PPF-Ciudadano Electrónico y DNI 4.0"  
date: 2020-11-12  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/152.-ciudadano-eelctronico-dni-4.0/152.CiudadanoEelctronicoDNI4.0  
tags: [ JPOD, Asociación Podcast, Covid, Médicos, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os invito a escuchar el podcast y ver la pagina de Ciudadano Electrónico además de comentar las novedades del DNI 4.0.  

Feed de [https://ciudadanoelectronico.es/](https://ciudadanoelectronico.es/)

[https://www.ivoox.com/podcast-ciudadano-electronico_fg_f1577761_filtro_1.xml](https://www.ivoox.com/podcast-ciudadano-electronico_fg_f1577761_filtro_1.xml)


<audio controls>
  <source src="https://archive.org/download/152.-ciudadano-eelctronico-dni-4.0/152.CiudadanoEelctronicoDNI4.0.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
