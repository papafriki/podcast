---
layout: post  
title: "Mi primer directo"  
date: 2018-11-26  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/053.MiPrimerDirecto/053.MiPrimerDirecto  
tags: [ Cervezas y Directos, Diógenes Digital, Papá Friki]  
comments: true 
---
Buenas muchachada, llegó el 24 de noviembre y participé en el directo que hizo Diógenes Digital en Cervezas y Directos en Alcobendas... Y luego debuté en directo con un programa hablando sobre clientes de mensajería junto a @Borrachuzo @micropakito @Aiondawn  

Muchas gracias por darme la oportunidad de estrenarme en un directo y pensar en mi para participar.

Os recuerdo el cartel de Cervezas y Directos: 

+ [Diogenes Digital](http://www.ivoox.com/p_sq_f1339791_1.html)
+ [Invita la casa](http://www.ivoox.com/p_sq_f199207_1.html)
+ [No hay cine sin palomitas](http://www.ivoox.com/p_sq_f1178206_1.html)
+ [Histocast](http://www.ivoox.com/p_sq_f132047_1.html)


<audio controls>
  <source src="https://archive.org/download/053.MiPrimerDirecto/053.MiPrimerDirecto.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
