---
layout: post  
title: "PPF-Casualidades, famosos, freidoras de aire y otro sorteo"  
date: 2021-06-09  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/185.-casualidades-famosos-freidoras-sorteos/185.CasualidadesFamososFreidorasSorteos  
tags: [Famosos, Albeto Galaso, Fernando Onega, Casualidades, Parejo, Proscenic T21, Sorteos, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os hablo de las casualidades, de famosos, de freidoras de aire y  de otro sorteo añadido a la lista.  


<audio controls>
  <source src="https://archive.org/download/185.-casualidades-famosos-freidoras-sorteos/185.CasualidadesFamososFreidorasSorteos.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
