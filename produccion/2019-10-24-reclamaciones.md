---
layout: post  
title: "Reclamaciones"  
date: 2019-10-24  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/Reclamaciones/Reclamaciones  
tags: [Citroen, MaratonPod, Reclamaciones, DNI, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os comento que ya tengo el citroen en casa. Os cuento que no he reclamdao pero a mí si me han puesto una reclamación el otro dia.  

[https://diario.mosqueteroweb.eu/2019/10/problema-de-dos-horas-de-retraso-en.html](https://diario.mosqueteroweb.eu/2019/10/problema-de-dos-horas-de-retraso-en.html)


<audio controls>
  <source src="https://archive.org/download/Reclamaciones/Reclamaciones.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/)
