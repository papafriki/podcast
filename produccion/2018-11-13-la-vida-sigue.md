---
layout: post  
title: "La vida sigue"  
date: 2018-11-13  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/050.LaVidaSigue/050.LaVidaSigue  
tags: [ Cervezas y Directos, Macbook Pro, Nexus 4, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento en qué ando liado ahora, antes de la operación de hernia umbilical de Leire mañana miercoles 14. El programa que he usado para hacer la imagen del S.O. El Capitán en el pincho usb es [Transmac](https://www.acutesystems.com/scrtm.htm)  

Os recuerdo el cartel de Cervezas y Directos el próximo 24 de noviembre de 2018 en Alcobendas (Madrid): 

+ [Diogenes Digital](http://www.ivoox.com/p_sq_f1339791_1.html)
+ [Invita la casa](http://www.ivoox.com/p_sq_f199207_1.html)
+ [No hay cine sin palomitas](http://www.ivoox.com/p_sq_f1178206_1.html)
+ [Histocast](http://www.ivoox.com/p_sq_f132047_1.html)

<audio controls>
  <source src="https://archive.org/download/050.LaVidaSigue/050.LaVidaSigue.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
