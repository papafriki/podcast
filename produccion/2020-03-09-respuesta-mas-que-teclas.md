---
layout: post  
title: "Respuesta Mas que teclas"  
date: 2020-03-08  
categories: podcast
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/120.respuestamasqueteclas/120.RespuestaMasQueTeclas  
tags: [DNIe, Certificados, Mas que teclas, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy contesto un audio de Mas que teclas sobre los certificados electrónicos del DNIe.  


Área de descargas
https://www.dnielectronico.es/PortalDNIe/PRF1_Cons02.action?pag=REF_1100

Instalador para 32bits en windows
https://www.dnielectronico.es/descargas/CSP_para_Sistemas_Windows/Windows_32_bits/DNIe_v14_0_2(32bits).exe

Instalador para 64bits en windows
https://www.dnielectronico.es/descargas/CSP_para_Sistemas_Windows/Windows_64_bits/DNIe_v14_0_2(64bits).exe


<audio controls>
  <source src="https://archive.org/download/120.respuestamasqueteclas/120.RespuestaMasQueTeclas.mp3" type="audio/mpeg">
</audio>
