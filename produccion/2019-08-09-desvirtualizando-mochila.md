---
layout: post  
title: "Desvirtualizando y mochila tecnológica en verano"  
date: 2019-08-09  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/089.desvirtualizandomochilatecnologica/089.DesvirtualizandoMochilaTecnologica  
tags: [Alfonso, Desvirtualizando, Mochila tecnológica, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os comento que hemos pasado un día con Alfonso, del podcast El Canon. Y os explico mi mochila tecnológica de éstas vacaciones.  


<audio controls>
  <source src="https://archive.org/download/089.desvirtualizandomochilatecnologica/089.DesvirtualizandoMochilaTecnologica.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/)
