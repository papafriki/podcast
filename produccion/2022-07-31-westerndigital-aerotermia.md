---
layout: post  
title: "PPF-Western Digital y Aerotermia"  
date: 2022-07-31  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/244.-wester-digital-aerotermia/244.WesterDigitalAerotermia  
tags: [Western Digital, NAS, Fallo, Aerotermia, Suelo radiante, Suelo refrescante, Placas solares, Papá Friki]  
comments: true 
---
Buenas muchacha hoy os cuento mas sobre el problema con el disco duro "NAS" de la marca Western Digital y trato de explicar mejor la aerotermia y el suelo radiante-refrescante.  

<br><br>
Podcast de la semana: Monos del espacio -- Sin vergüenza
<br><br>
[https://www.ivoox.com/mde-sin-verguenza-audios-mp3_rf_90062286_1.html](https://www.ivoox.com/mde-sin-verguenza-audios-mp3_rf_90062286_1.html)
<br><br>


<br>
<audio controls>
  <source src="https://archive.org/download/244.-wester-digital-aerotermia/244.WesterDigitalAerotermia.mp3" type="audio/mpeg">  
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>
