---
layout: post  
title: "Kedadas JPOD"  
date: 2018-10-04  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/043.KedadasJPOD/043.KedadasJPOD  
tags: [ JPOD, Kedadas, Papá Friki]  
comments: true 
---
 Buenas muchachada, hoy os cuento las dos Kedadas a las que quiero asistir en las JPOD 2018 de éste fin de semana en Madrid.  

Sábado a las 11:00 en la Cerveceria Chamberí en la calle Luchana nº 36.

Sábado a partir de las 13:00 en el Panaria de la calle Santa Engracia 45.

Mas que nunca, nos vemos, nos leemos, nos escuchamos!  

Referencias del episodio de hoy:

+ [JPOD](http://jpod.es/)

<audio controls>
  <source src="https://archive.org/download/043.KedadasJPOD/043.KedadasJPOD.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
