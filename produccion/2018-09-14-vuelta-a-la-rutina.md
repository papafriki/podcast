---
layout: post  
title: "Vuelta a la rutina"  
date: 2018-09-14  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/040.VueltaALaRutina/040.VueltaALaRutina  
tags: [ Centros Comerciales, Instagram, Podcast Lobo, Papá Friki]  
comments: true 
---
Buenas muchachada, os comento la vuelta a la rutina que supone empezar las clases de los niños.  

Referencias del episodio de hoy:


+ [storiesig.com](https://storiesig.com/)
+ [Router mono Bluetooth](https://mono-bluetooth-router.es.aptoide.com/)
+ [Podcast Lobo](https://itunes.apple.com/es/podcast/lobo/id1260166820?l=en&mt=2&i=1000419245585)

<audio controls>
  <source src="https://archive.org/download/040.VueltaALaRutina/040.VueltaALaRutina.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
