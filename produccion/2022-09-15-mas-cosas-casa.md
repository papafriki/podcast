---
layout: post  
title: "PPF-Más cosas de casa"  
date: 2022-06-15  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/250.-mas-cosas-de-casa/250.MasCosasDeCasa  
tags: [Libros, Colegio, Digi, Telefonica, Popliteo, Menisco, D.N.I., Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento más cosas de casa.  


<br>
<audio controls>
  <source src="https://archive.org/download/250.-mas-cosas-de-casa/250.MasCosasDeCasa.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
