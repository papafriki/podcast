---
layout: post  
title: "Oyendo nuevos podcast"  
date: 2020-01-14  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/112.oyendonuevospodcast/112.OyendoNuevosPodcast  
tags: [Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os traigo unos cuantos podcast que he descubierto en los últimos días:  

**Andres Ramos Podcast**
+ [https://www.ivoox.com/escuchar-audios-andres-ramos_al_19191640_1.html](https://www.ivoox.com/escuchar-audios-andres-ramos_al_19191640_1.html)

**Alto y Claro**
+ [https://www.ivoox.com/podcast-alto-claro_fg_f1781833_filtro_1.xml](https://www.ivoox.com/podcast-alto-claro_fg_f1781833_filtro_1.xml)

**Frente al cliente**
+ [https://www.ivoox.com/podcast-frente-al-cliente_sq_f1819687_1.html](https://www.ivoox.com/podcast-frente-al-cliente_sq_f1819687_1.html)

**Cool Copetins**
+ [https://www.ivoox.com/podcast-coolcopetins_fg_f1641406_filtro_1.xml](https://www.ivoox.com/podcast-coolcopetins_fg_f1641406_filtro_1.xml)

**Por tierra mar y arie**
+ [https://www.ivoox.com/podcast-portierramaryaire-podcast_fg_f1456223_filtro_1.xml](https://www.ivoox.com/podcast-portierramaryaire-podcast_fg_f1456223_filtro_1.xml)

<audio controls>
  <source src="https://archive.org/download/112.oyendonuevospodcast/112.OyendoNuevosPodcast.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
