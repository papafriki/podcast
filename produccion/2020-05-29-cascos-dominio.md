---
layout: post  
title: "PPF-Cascos bluetooth y dominio"  
date: 2020-05-29  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/130.-cascos-bluetooth-dominio/130.CascosBluetoothDominio  
tags: [ Bluetooth, Despistado, Dominio, Papá Friki]  
comments: true 
---
Buenas muchachada hoy repaso los cascos bluetooth que he tenido y os presento el dominio www.papafriki.es.  


Entrada explicando instalación de docker y pihole en raspberry pi:

[https://papafriki.gitlab.io/podcast/pihole/](https://papafriki.gitlab.io/podcast/pihole/)

<audio controls>
  <source src="https://archive.org/download/130.-cascos-bluetooth-dominio/130.CascosBluetoothDominio.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)