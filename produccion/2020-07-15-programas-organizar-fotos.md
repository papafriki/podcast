---
layout: post  
title: "PPF-Programas para organizar fotos"  
date: 2020-07-15  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/136.-programas-orgagnizar-fotos/136.ProgramasOrgagnizarFotos  
tags: [  Organizar, Fotografia, Windows, Programas, Edición, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento los programas que uso para ayudarme a clasificar las fotos en local.  


Photo Move

[https://www.mjbpix.com/automatically-move-photos-to-directories-or-folders-based-on-exif-date/](https://www.mjbpix.com/automatically-move-photos-to-directories-or-folders-based-on-exif-date/)

Se me pasó comentar que necesita de registro, pero puedes dar un mail basura, basta con tener apuntado el correo y la contraseña que te facilitan en un txt para introducirlo cuando sea necesario.


Advanced Renamer

[https://www.advancedrenamer.com/user_guide/gettingstarted](https://www.advancedrenamer.com/user_guide/gettingstarted)

<audio controls>
  <source src="https://archive.org/download/136.-programas-orgagnizar-fotos/136.ProgramasOrgagnizarFotos.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
