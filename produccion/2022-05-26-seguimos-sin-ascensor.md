---
layout: post  
title: "PPF-Seguimos sin ascensor"  
date: 2022-05-26   
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/235.-seguimos-sin-ascensor/235.SeguimosSinAscensor  
tags: [ Puerta, Ascensor, Garaje, Expdición D.N.I.,Coche, Thinkpad, Lenovo, Papá Friki]  
comments: true 
---
Buenas muchachada hoy un poco de todo.  

<br><br>
Tweet #TipsConsejosDelDNI
<br>
[https://twitter.com/papa_friki/status/1528133757101912065](https://twitter.com/papa_friki/status/1528133757101912065)



<br>
<audio controls>
  <source src="https://archive.org/download/235.-seguimos-sin-ascensor/235.SeguimosSinAscensor.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>
