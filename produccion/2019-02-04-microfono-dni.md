---
layout: post  
title: "Prueba de micrófono y anecdotas DNI"  
date: 2019-02-04  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/063.PruebaBoyaAnecdotasDni/063.PruebaBoyaAnecdotasDni  
tags: [ Micrófono, Boya, DNI, Papá Friki]  
comments: true 
---
Buenas muchachada, os comparto una prueba real del micrófono Boya mientras volvía a casa en el coche... El audio no sirve para publicarlo directamente y lo he intentado limpiar con Audacity, esto es lo mejor que he logrado. De paso os he contado varias anécdotas de una mañana trabajando en el DNI.  


<audio controls>
  <source src="https://archive.org/download/063.PruebaBoyaAnecdotasDni/063.PruebaBoyaAnecdotasDni.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>
