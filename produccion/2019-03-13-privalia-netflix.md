---
layout: post  
title: "Privalia, Netflix y El Corte Inglés"  
date: 2019-03-13  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/068.PrivaliaNetflix/068.PrivaliaNetflix  
tags: [ Podcast Addict, Papá Friki]    
comments: true 
---
Buenas muchachada, hoy os comento los procesos de alta mal implementados en algunas plataformas y los problemas que pueden ocasionar.  

Los métodos de contacto:
Twitter: https://twitter.com/papa_friki
Correo: papafrikipodast@gmail.com
Web: https://papafriki.gitlab.io/podcast
Feed Podcast: http://feeds.feedburner.com/papafriki
Sumando Podcast: https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml  

<audio controls>
  <source src="https://archive.org/download/068.PrivaliaNetflix/068.PrivaliaNetflix.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>
