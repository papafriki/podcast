---
layout: post  
title: "PPF-Segunda dosis y variado"  
date: 2021-06-02  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/184.-segunda-dosis/184.SegundaDosis  
tags: [Fenix, Vacuna, Coca-Cola,D.N.I. 4.0, Chicago Fire, Invencible, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre la segunda dosis de la vacuna, consumo de coca-cola, aplicación Fenix, el D.n.I. 4.0 vamos un variado en toda regla.  



<audio controls>
  <source src="https://archive.org/download/184.-segunda-dosis/184.SegundaDosis.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
