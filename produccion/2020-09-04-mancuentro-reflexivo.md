---
layout: post  
title: "PPF-Mancuentro reflexivo cumplo 40"  
date: 2020-09-04  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/142.-mancuentro-reflexivo-cumple-40/142.MancuentroReflexivoCumple40  
tags: [  Cumpleaños, Mancuentro, Sospechosos habituales, Vida, Cuarenton, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento que "mancuentro" reflexivo y que es mi 40 cumpleaños.  


<audio controls>
  <source src="https://archive.org/download/142.-mancuentro-reflexivo-cumple-40/142.MancuentroReflexivoCumple40.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
