---
layout: post  
title: "PPF-Seguridad en Linux y enganchada en el D.N.I."  
date: 2021-02-25  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/169.-seguridad-linux-enganchada-dni/169.SeguridadLinuxEnganchadaDNI  
tags: [Linux, Lubuntu. Debian, Correos, D.N.I., Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento como saltarse la contraseña del usuario en linux y una enganchada con una madre en el D.N.I.  


Artículo explicando como saltarse las contraseñas en Linux

[https://itsfoss.com/how-to-hack-ubuntu-password/](https://itsfoss.com/how-to-hack-ubuntu-password/)

<audio controls>
  <source src="https://archive.org/download/169.-seguridad-linux-enganchada-dni/169.SeguridadLinuxEnganchadaDNI.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
