---
layout: post  
title: "PPF-Movistar y anécdotas del D.N.I."  
date: 2021-07-21  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/190.-movistar-anecdotas-dni/190.MovistarAnecdotasDNI  
tags: [Movistar, Problemas, D.N.I., Anécdotas, Series, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento unos cargos de movistar y más anécdotas del D.N.I.  


Podcast de la semana: Charlas con aita

[https://charlasconaita.ciudadanoio.com/feed/podcast/](https://charlasconaita.ciudadanoio.com/feed/podcast/)

<audio controls>
  <source src="https://archive.org/download/190.-movistar-anecdotas-dni/190.MovistarAnecdotasDNI.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
