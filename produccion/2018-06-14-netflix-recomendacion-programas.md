---
layout: post  
title: "Netflix y recomendación de programas"  
date: 2018-06-14  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/022.NetflixRecomendacionProgramas/022.NetflixRecomendacionProgramas  
tags: [ Orange, CGNat, Raspberry Pi, Zerotier]  
comments: true 
---
Buenas muchachada, hoy os cuento los problemas de audio que estoy teniendo en el Chromecast y os recomiendo tres programas, que espero os sean útiles.  

Os confirmo que el programa Mobdro únicamente trabaja con ficheros jpg. 

Referencias del episodio de hoy:   
+ [Metanull](https://github.com/nikvoronin/Metanull)  
+ [Mobdro](http://mobdro.es/)  
+ [Caja de música](https://musiclab.chromeexperiments.com/Song-Maker/)  
+ [La razón de La Voz](http://larazondelavoz.gitlab.io/)  


<audio controls>
  <source src="https://archive.org/download/022.NetflixRecomendacionProgramas/022.NetflixRecomendacionProgramas.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
