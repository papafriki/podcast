---
layout: post  
title: "RGPD, coche y cliente Fenix 2 for Twitter"  
date: 2018-05-29  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/019.RGPDCocheFenix/019.RGPDCocheFenix  
tags: [RGPD, Twitter, Fenix, Podcast, Citroen, Papá Friki]  
comments: true 
---
Buenas muchachada, en el episodio de hoy os comento un caso curioso con la RGPD, os cuento que ya tengo el coche y porque estoy con la mosca detrás de la oreja. Además estoy probando la aplicación Fenix 2 for Twitter y os cuento mi experiencia.  

![](https://i.imgur.com/seqmQIq.png)


Referencias del episodio de hoy:   
+ [The Geek Cow](https://twitter.com/D_Silgo)  
+ [Aplicación Fenix 2 for Twitter](https://play.google.com/store/apps/details?id=it.mvilla.android.fenix2)  
+ [Thor_4](https://twitter.com/thor_4)

<audio controls>
  <source src="https://archive.org/download/019.RGPDCocheFenix/019.RGPDCocheFenix.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
