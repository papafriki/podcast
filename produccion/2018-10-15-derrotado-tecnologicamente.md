---
layout: post  
title: "Derrotado tecnológicamente"  
date: 2018-10-15  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/045.DerrotadoTecnologicamente/045.DerrotadoTecnologicamente    
tags: [ Homeassistant, respeto, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os comento como he sido derrotado tecnológicamente y unas faltas de respeto en momentos muy poco oportunos.  



<audio controls>
  <source src="https://archive.org/download/045.DerrotadoTecnologicamente/045.DerrotadoTecnologicamente.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
