---
layout: post  
title: "Lenguaje inclusivo"  
date: 2019-02-13  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/064.LenguajeInclusivo/064.LenguajeInclusivo  
tags: [ Lenguaje, Boya, Anécdotas, DNI, Papá Friki]  
comments: true 
---
Buenas muchachada, os comparto unas reflexiones nocturnas. Por cierto los ruidos que se oyen son roces del micrófono Boya con la ropa... tendré que empezar a grabar desnudo ;-)  

<audio controls>
  <source src="https://archive.org/download/064.LenguajeInclusivo/064.LenguajeInclusivo.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>
