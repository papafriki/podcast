---
layout: post  
title: "Vacaciones tecnológicas"  
date: 2018-08-28  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/038.VacacionesTecnologicas/038.VacacionesTecnologicas  
tags: [ lowi, vacaciones, tecnologia, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento el uso de la tecnologia durante mis vacaciones.  


Hago aquí una corrección y es que cuando hablo del portátil y digo que me gusta el navegador Dolphin, me queria referir al explorador de ficheros Dolphin.


<audio controls>
  <source src="https://archive.org/download/038.VacacionesTecnologicas/038.VacacionesTecnologicas.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
