---
layout: post  
title: "PPF-Mas de la casa y del D.N.I."  
date: 2022-12-20  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/265.-casa-dni/265.CasaDNI  
tags: [ Aerotermia, Gotera, Terraza, D.N.I., TPV, Tarjeta, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os hablo mas de la casa, aerotermia y del D.N.I.  


<br><br>
Charlando con... un trabajador de Michelin
[https://spotifyanchor-web.app.link/e/k9j9bD5m1ub](https://spotifyanchor-web.app.link/e/k9j9bD5m1ub)
<br>

<br>
<audio controls>
  <source src="https://archive.org/download/265.-casa-dni/265.CasaDNI.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>

