---
layout: post  
title: "PPF-Proyecto iPad"  
date: 2021-09-08  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/198.-proyectoi-pad/198.ProyectoiPad  
tags: [iPad, Apple Pencil, Capsulas, Tecnocincuentones, Cruel Summer, Looki, Amazon, Reacondicionados, Disney, Registro Civil, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre el proyecto iPad del colegio, series y nueva ley del Registro Civil.  

Podcast de la semana: Capsulas de Isanshade -- Los tres bastones 

https://www.ivoox.com/02-los-tres-bastones-audios-mp3_rf_73532220_1.html



Tecnocincuentones:  Recordemos 2001


https://www.ivoox.com/t50-episodio-150-recordemos-2001-audios-mp3_rf_75035902_1.html


<audio controls>
  <source src="https://archive.org/download/198.-proyectoi-pad/198.ProyectoiPad.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
