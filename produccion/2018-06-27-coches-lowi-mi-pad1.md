---
layout: post  
title: "Entre coches, Lowi y Mi Pad 1 anda el juego"  
date: 2018-06-27  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/024.CochesLowiMiPad/024.CochesLowiMiPad.mp3  
tags: [Coches, Lowi, Mi Pad 1, Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada, en el episodio de hoy os comento como quedan los coches de la familia, la imposibilidad de conectar a la red de datos de Lowi  en el Zopo 530+ y la compra de la Mi Pad 1 de segunda mano a Thor4.  

![](https://i.imgur.com/cUJ8sSP.png)

Referencias del episodio de hoy:   
+ [Lowi](https://www.lowi.es/)  
+ [Thor_4](https://twitter.com/thor_4)

<audio controls>
  <source src="https://archive.org/download/024.CochesLowiMiPad/024.CochesLowiMiPad.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
