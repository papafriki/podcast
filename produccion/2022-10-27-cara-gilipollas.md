---
layout: post  
title: "PPF-Cara de gilipollas"  
date: 2022-10-17  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/257.-cara-gilipollas/257.CaraGilipollas  
tags: [Gilipollas, Eureka, Putty, Keys, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre esa cara de gilipollas que se te queda cuando te das cuenta de un fallo estúpido.  


<br><br>
Podcast de Deshaogo Geek
[https://www.ivoox.com/podcast-desahogo-geek_sq_f11202056_1.html](https://www.ivoox.com/podcast-desahogo-geek_sq_f11202056_1.html)

<br>
<audio controls>
  <source src="https://archive.org/download/257.-cara-gilipollas/257.CaraGilipollas.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>



