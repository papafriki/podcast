---
layout: post  
title: "Mini vacaciones, Lowi y variado"  
date: 2018-06-21  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/023.VacacionesLowiVariado/023.VacacionesLowiVariado  
tags: [Lowi, Eduardo Collado, Elias Gomez, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento de nuestras mini vacaciones, que me he dado de alta en Lowi para aprovechar el bono de 60 Gb para el verano.  

![](https://i.imgur.com/XUHPFOh.jpg)  

Referencias del episodio de hoy:   
+ [Eduardo Collado](https://www.eduardocollado.com/)  
+ [Elias Gomez Reflexiones de un Geek desde Bilbao](http://eliasgomez.pro/episodio/reflexiones-de-un-geek-desde-bilbao/171-la-app-de-laliga-te-espia-si-se-lo-pides-y-google-home-en-espan%CC%83ol/)  
+ [Lowi](https://www.lowi.es/)  


<audio controls>
  <source src="https://archive.org/download/023.VacacionesLowiVariado/023.VacacionesLowiVariado.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
