---
layout: post  
title: "PPF-Fin del verano"  
date: 2022-08-31  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/249.-fin-del-verano/249.FinDelVerano  
tags: [Feedlipinos, Loteria, Amazon, Urgencias, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento las ultimas anécdotas del verano.  

<br><br>
Podcast de la semana: Los últimos de feedlipinas
<br><br>
[https://ultimosfeed.blogspot.com/](https://ultimosfeed.blogspot.com/)
<br><br>


<br>
<audio controls>
  <source src="https://archive.org/download/249.-fin-del-verano/249.FinDelVerano.mp3" type="audio/mpeg">  
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>
