---
layout: post  
title: "PPF-Gracias David y fotos duplicadas"  
date: 2020-06-22  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/133.-gracias-david-aplicacion-fotos-duplicadas/133.GraciasDavidAplicacionFotosDuplicadas  
tags: [  David, Surface, Fotos, Duplicados, Papá Friki]  
comments: true 
---
Buenas muchachada hoy toca darles las gracias a David Silgo y os comento una aplicación que estoy probando para localizar fotos duplicadas en el equipo.  

El programa que comento para buscar fotos duplicadas 

[https://wiki.eurek.org/find-same-image-ok-encuentra-y-borra-imagenes-duplicadas/](El programa que comentohttps://wiki.eurek.org/find-same-image-ok-encuentra-y-borra-imagenes-duplicadas/)


<audio controls>
  <source src="https://archive.org/download/133.-gracias-david-aplicacion-fotos-duplicadas/133.GraciasDavidAplicacionFotosDuplicadas.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
