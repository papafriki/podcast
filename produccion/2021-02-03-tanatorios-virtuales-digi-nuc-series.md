---
layout: post  
title: "PPF-Tanatorios virtuales, Digi, series y gracias David"  
date: 2021-02-03  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/165.-tanatorios-virtuales-digi-series/165.TanatoriosVirtualesDigiSeries  
tags: [ Tanatorio, Virtual, Series, Digi, PPPoe, Nuc, David, GeekCow, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento un servicio de condolencias online o tanatorio virtual, os cuento como llevo el tema del PPPOe de Digi y os comento de forma "magistral y profesional" las últimas series que hemos visto.  

Tanatorio Virtual

[https://surecuerdo.com/](https://surecuerdo.com/)

El directorio de la Asociación Podcast

[https://asociacionpodcast.es/directorio/](https://asociacionpodcast.es/directorio/)


Si te registras y usas el código AG2Q02 tendrás 5€ de descuento el primer año.

[https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02](https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02)

<audio controls>
  <source src="https://archive.org/download/165.-tanatorios-virtuales-digi-series/165.TanatoriosVirtualesDigiSeries.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
