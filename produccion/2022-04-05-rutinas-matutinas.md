---
layout: post  
title: "PPF-Aniversario, timos en la luz y pasaportes"  
date: 2022-04-05 10:00:00  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/228.-rutinas-matutinas/228.RutinasMatutinas  
tags: [Miguel Angel Cabrera, Rutinas, Ejercicio, Wordle, Pistachos, Anécdotas, DNI, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre mis rutinas matutinas.  



Podcast de la semana: Frente al cliente -- Bilinguismo
<br>
[https://www.ivoox.com/bilinguismo-audios-mp3_rf_84854022_1.html](https://www.ivoox.com/bilinguismo-audios-mp3_rf_84854022_1.html)



<br>
<audio controls>
  <source src="https://archive.org/download/228.-rutinas-matutinas/228.RutinasMatutinas.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
