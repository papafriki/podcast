---
layout: post  
title: "PPF-Retomando la domótica, directorio de la Asociación Podcast y  bienvenida a Dailos"  
date: 2021-01-21  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/163.-retomando-domotica-directorio-asociacion-dailos/163.RetomandoDomoticaDirectorioAsociacionDailos  
tags: [ Domótica, HomeAssistant, Asociación Podcast, Dailos, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento que he retomado la domótica, celebrando el nuevo directorio de la Asociación Podcast y  damos la bienvenida a Dailos y su podcast "Desde mi Noray".  


El podcast de Dailos "Desde mi Noray"
[https://anchor.fm/desdeminoray](https://anchor.fm/desdeminoray)

El directorio de la Asociación Podcast
[https://asociacionpodcast.es/directorio/](https://asociacionpodcast.es/directorio/)


Si te registras y usas el código AG2Q02 tendrás 5€ de descuento el primer año.
[https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02](https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02)

<audio controls>
  <source src="https://archive.org/download/163.-retomando-domotica-directorio-asociacion-dailos/163.RetomandoDomoticaDirectorioAsociacionDailos.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
