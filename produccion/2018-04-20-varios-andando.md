---
layout: post  
title: "Varios mientras ando"  
date: 2018-04-20  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/013.Andando/013.Andando.mp3  
tags: [meetup, domology, podcast, papa friki]  
comments: true 
---
Buenas muchachada, hoy me acompañáis unos minutos mientras ando de camino al trabajo y os comento algo más de los meetup, de la GearS2 y de la mochila de Xiaomi.  

![](https://papafriki.gitlab.io/podcast/images/logo.png)


<audio controls>
  <source src="https://archive.org/download/012.Meetup/012.Meetup.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>