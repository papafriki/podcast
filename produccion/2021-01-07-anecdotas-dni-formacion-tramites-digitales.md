---
layout: post  
title: "PPF-Anécdotas DNI y formación trámtites digitales"  
date: 2021-01-07  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/161.-anecdotas-dniformacion-ciudadanos-electronicos/161.AnecdotasDNIFormacionCiudadanosElectronicos  
tags: [ Anécdotas, Educación, Formación,Trámites, Certificados, Windows, DNI, Papá Friki]  
comments: true 
---
Buenas muchahcada, hoy os comento unas anecdotas del dni y hago un reflexión sobre la formación a los ciudadanos electrónicos.  


<audio controls>
  <source src="https://archive.org/download/161.-anecdotas-dniformacion-ciudadanos-electronicos/161.AnecdotasDNIFormacionCiudadanosElectronicos.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
