---
layout: post  
title: "PPF-Cambio de casa"  
date: 2022-03-23  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/227.-cambio-de-casa/227.CambioDeCasa  
tags: [Casa, Bancos, Euribor, Domótica, Cambio, Mudanza, D.N.I., Anécdotas, Cáncer de mama metastásico, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento que tenemos previsto cambiarnos de casa y alguna cosilla más del D.N.I.  



Podcast de la semana: Monos del Espacio -- www.cancermamametastasico.es
<br>
[http://wintablet.info/podcast/mde-acmm/](http://wintablet.info/podcast/mde-acmm/)
<br>

<br>

<audio controls>
  <source src="https://archive.org/download/227.-cambio-de-casa/227.CambioDeCasa.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
