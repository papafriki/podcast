---
layout: post  
title: "PPF-Compra cascos en Amazon y más anécdotas de D.N.I"  
date: 2020-07-21  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/137.-cascos-amazon-anecdotas-dni/137.CascosAmazonAnecdotasDNI  
tags: [  Organizar, Fotografia, Windows, Programas, Amazon, Cascos, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento unos cascos que compré en Amazon y más anécdotas del D.N.I.  


Amok Exif Sorter

[http://www.amok.am/en/freeware/amok_exif_sorter/download/](http://www.amok.am/en/freeware/amok_exif_sorter/download/)


<audio controls>
  <source src="https://archive.org/download/137.-cascos-amazon-anecdotas-dni/137.CascosAmazonAnecdotasDNI.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)