---
layout: post  
title: "Aniversario"  
date: 2019-03-10  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/067.Aniversario/067.Aniversario  
tags: [ Privalia, Netflix, El Corte Ingles, Papá Friki]    
comments: true 
---
Buenas muchachada, hoy repaso junto a vosotros mi año con el podcast.  

<audio controls>
  <source src="https://archive.org/download/067.Aniversario/067.Aniversario.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>
