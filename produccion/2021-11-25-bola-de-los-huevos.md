---
layout: post  
title: "PPF-La bola de los huevos"  
date: 2021-11-25  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/209.-boladelos-huevos/209.BoladelosHuevos  
tags: [Citroen, C4, Romolque, Bola, Talleres, ITV, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento la historia de la bola de romolque que hemos puesto en el Citroen C4.  



Si te registras y usas el código AG2Q02 tendrás 5€ de descuento el primer año.

[https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02](https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02)


<audio controls>
  <source src="https://archive.org/download/209.-boladelos-huevos/209.BoladelosHuevos.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
