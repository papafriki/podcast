---
layout: post  
title: "Orange y su CGNat"  
date: 2018-06-07  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/021.OrangeCGNat/021.OrangeCGNat  
tags: [ Orange, CGNat, Raspberry Pi, Zerotier]  
comments: true 
---
Buenas muchachada, en el episodio de hoy os cuento que me han metido tras el CGNat de Orange,  que problemas acarrea y la solución que he encontrado.  


Referencias del episodio de hoy:   
+ [uGeek](https://ugeek.github.io)  
+ [Eduardo Collado](https://www.eduardocollado.com/)  
+ [Naseros](https://naseros.com/)  
+ [Zerotier](https://www.zerotier.com)  
+ [Instalación Zerotier en Raspberry Pi](https://iamkelv.in/blog/2017/06/zerotier.html)  


<audio controls>
  <source src="https://archive.org/download/021.OrangeCGNat/021.OrangeCGNat.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
