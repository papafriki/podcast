---
layout: post  
title: "PPF-Fin de semana de JPOD y Asociación podcast"  
date: 2020-11-20  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/153.-fin-de-semana-jpod/153.FinDeSemanaJpod  
tags: [Ciudadano Electrónico, DNI 4.0, newsletter, Anécdotas, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento el fin de semana sin jpod sin apenas poder seguirlas y os comento el programa de referidos de la Asociación Podcast.  

Si te registras y usas el codigo AG2Q02 tendrás 5€ de descuento el primer año. 

[https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02](https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02)


<audio controls>
  <source src="https://archive.org/download/153.-fin-de-semana-jpod/153.FinDeSemanaJpod.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
