---
layout: post  
title: "PPF-Vacunado y uso de whatsapp por la gente mayor"  
date: 2021-03-02  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/170.-vacunado-whatsapp/170.VacunadoWhatsapp  
tags: [Correos, Vacuna, Astrazeneca, Covid, Whatsapp, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento que estoy vacunado y el uso de whatsapp por parte de la gente mayor.  




<audio controls>
  <source src="https://archive.org/download/170.-vacunado-whatsapp/170.VacunadoWhatsapp.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
