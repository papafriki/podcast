---
layout: post  
title: "PPF-Google Rewards y anécdotas del D.N.I."  
date: 2020-12-16  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/158.-google-rewards-anecdotas-dni/158.GoogleRewardsAnecdotasDNI  
tags: [Google, Rewards, Anécdotas, DNI, Stadia, Papá Friki]  
comments: true 
---
Buenas muchada hoy os comento unos lios que tengo en las cuentas de Google y más anécdotas del dni.  


Aplicación para pedir cita previa del D.N.I. en Android

[https://play.google.com/store/apps/details?id=com.dgpcitaprevia2&hl=es&gl=US](https://play.google.com/store/apps/details?id=com.dgpcitaprevia2&hl=es&gl=US)

<audio controls>
  <source src="https://archive.org/download/158.-google-rewards-anecdotas-dni/158.GoogleRewardsAnecdotasDNI.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
