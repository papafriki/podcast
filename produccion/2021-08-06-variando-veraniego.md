---
layout: post  
title: "PPF-Variado veraniego"  
date: 2021-08-06  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/192.-variado-veraniego/192.VariadoVeraniego  
tags: [Verano, Deporte, Mochila tecnológica, Anécdotas, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os traigo un variado veraniego con deporte, mochila tecnológica y anecdotas del D.N.  


Podcast de la semana: Desde mi noray

[https://www.ivoox.com/mas-sobre-telegram-audios-mp3_rf_73166909_1.html](https://www.ivoox.com/mas-sobre-telegram-audios-mp3_rf_73166909_1.html)


Especial Pildoras de Isanshade

[https://www.ivoox.com/podcast-capsulas_fg_f1668858_filtro_1.xml](https://www.ivoox.com/podcast-capsulas_fg_f1668858_filtro_1.xml)

<audio controls>
  <source src="https://archive.org/download/192.-variado-veraniego/192.VariadoVeraniego.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
