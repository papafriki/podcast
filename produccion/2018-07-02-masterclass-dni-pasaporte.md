---
layout: post  
title: "Masterclass de D.N.I. y Pasaportes"  
date: 2018-07-02  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/025.MasterClassDNIPasaporte/025.MasterClassDNIPasaporte  
tags: [DNI, Pasaporte, Ángel, Anécdotas, Tertulia, Papá Friki]  
comments: true 
---
Buenas muchachada, en el episodio especial  de hoy, os comento junto con Angel, el tramite de expedición del D.N.I. y Pasaporte español, documentación necesaria y pequeñas anecdotas.  

Nuevamente os pido disculpas por la calidad del audio y prometemos mejorarlo en la segunda entrega que tenemos pensado hacer.

![](https://i.imgur.com/PwQUxSW.png)

Referencias del episodio de hoy:   
+ [www.citapreviadnie.es](https://www.citapreviadnie.es/citaPreviaDniExp/)
+ [Portal del DNI y Pasaporte](https://www.dnielectronico.es/PortalDNIe/)
+ [Ministerio de Justicia Partida Literal de Nacimiento](https://sede.mjusticia.gob.es/cs/Satellite/Sede/es/tramites/certificado-nacimiento)
+ [Planeta Cuñao](http://planetacunao.com/)

<audio controls>
  <source src="https://archive.org/download/025.MasterClassDNIPasaporte/025.MasterClassDNIPasaporte.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
