---
layout: post  
title: "Cambio del feed y Gear S2 Sport"  
date: 2018-03-31  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/008.CambioFeedGearS2/008.CambioFeedGearS2    
tags: [feedburner,Samsung,GearS2,Podcast,Papa Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento como y porqué he cambiado el feed del podcast; también os comento la nueva actualización que ha tenido el Samsung Gear S2 Sport  y os hago un breve repaso de mi experiencia con los relojes inteligentes.  
<center>![](https://papafriki.gitlab.io/podcast/images/logo.png)</center>  
Referencias que  he mencionado durante el podcast de hoy:  
+ [EducandoGeek](https://educandogeek.github.io/) de @jgurillo.
+ [Diogenes Digital](https://www.ivoox.com/podcast-diogenes-digital_sq_f1339791_1.html) donde participa Microkpakito.
+ Grupo de Telegram [Gear S2 Sport](https://t.me/SamsungGearS2)

<audio controls>
  <source src="https://archive.org/download/008.CambioFeedGearS2/008.CambioFeedGearS2.mp3" type="audio/mpeg">
</audio>


Y recuerda, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
