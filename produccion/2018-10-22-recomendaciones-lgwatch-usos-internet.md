---
layout: post  
title: "Recomendaciones LG Urbane Watch y usos de internet"  
date: 2018-10-22  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/046.RecomendacionesLGUsosInternet/046.RecomendacionesLGUsosInternet  
tags: [ JPOD, Redes Sociales, Europol, Papá Friki]  
comments: true 
---
Buenas muchachada, os hago una recomendación de podcast para terminar el tema de las JPOD, os cuento que me he comprado de segunda mano el LG Watch Urbane y unos ejemplos de uso de internet.  

Referencias del episodio de hoy:


+ [Ya conoces las noticias](http://www.ivoox.com/p_sq_f1264885_1.html)
+ [Noticia trabajador despedido](https://www.elcomercio.es/aviles/despedido-aviles-celebrar-20181016185951-nt.html)
+ [Europol Stop Child Abuse](https://www.europol.europa.eu/stopchildabuse)

<audio controls>
  <source src="https://archive.org/download/046.RecomendacionesLGUsosInternet/046.RecomendacionesLGUsosInternet.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
