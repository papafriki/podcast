---
layout: post  
title: "PPF-Revisión anual tecnológica"  
date: 2021-12-22  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/213.-revision-tecnologica-21/213.RevisionTecnologica21  
tags: [Compras, Tecnología, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os hago mi revisión tecnológica del año.  
<br>

Podcast de la semana: Angel Martin -- Por si las voces vuelven
<br>
[https://open.spotify.com/show/3ZsoveYNKojRyaeWpM3ruo](https://open.spotify.com/show/3ZsoveYNKojRyaeWpM3ruo)
<br>
<audio controls>
  <source src="https://archive.org/download/213.-revision-tecnologica-21/213.RevisionTecnologica21.mp3" type="audio/mpeg">
</audio>
<br>
Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>
Os recuerdo, los métodos de contacto son:
<br>
+ Web: <http://www.papafriki.es><br>
+ Twitter: <https://twitter.com/papa_friki><br>
+ Correo: <papafrikipodcast@gmail.com><br>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en><br>
