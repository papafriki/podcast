---
layout: post  
title: "PPF-Anécdotas y aplicación oficial"  
date: 2022-06-30  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/240.-anecdotas-aplicacion/240.AnecdotasAplicacion  
tags: [Anécdotas, D.N.I., Aplicación, Android, Papá Friki]  
comments: true 
---
Buenas muchachada hoy traigo varias anécdotas del D.N.I. y una aplicación oficial para hacer trámites.  


Aplicación Acceso administración con DNIe o Dnie Login Widget
<br><br>
[https://play.google.com/store/apps/details?id=com.dnieloginwidget&hl=es](https://play.google.com/store/apps/details?id=com.dnieloginwidget&hl=es)


<br>
<audio controls>
  <source src="https://archive.org/download/240.-anecdotas-aplicacion/240.AnecdotasAplicacion.mp3" type="audio/mpeg">  
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>
