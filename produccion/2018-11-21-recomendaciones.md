---
layout: post  
title: "Recomendaciones de Podcast"  
date: 2018-11-21  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/052.Recomendaciones/052.Recomendaciones  
tags: [ Recomendaciones, Podcast, Voces en la caja, Victorsnk, Descargas en mi mente, Sumando Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada, junto a Leire hoy os hago unas recomendaciones de podcast en los que he participado últimamente y otros que he descubierto y empiezan ahora su andadura.  

Referencias del episodio de hoy:

+ [Descargas de mi mente](https://www.ivoox.com/descargas-mi-mente_fg_f1584288_filtro_1.xml)
+ [Victorsnk PODCAST](https://www.ivoox.com/victorsnk-podcast_fg_f1636666_filtro_1.xml)
+ [Voces en la caja](http://www.ivoox.com/p_sq_f1629420_1.html)
+ [Sumando Podcast](http://www.ivoox.com/p_sq_f1629420_1.html)

Os recuerdo el cartel de Cervezas y Directos el próximo 24 de noviembre de 2018 en Alcobendas (Madrid): 

+ [Diogenes Digital](http://www.ivoox.com/p_sq_f1339791_1.html)
+ [Invita la casa](http://www.ivoox.com/p_sq_f199207_1.html)
+ [No hay cine sin palomitas](http://www.ivoox.com/p_sq_f1178206_1.html)
+ [Histocast](http://www.ivoox.com/p_sq_f132047_1.html)


<audio controls>
  <source src="https://archive.org/download/052.Recomendaciones/052.Recomendaciones.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
