---
layout: post  
title: "Taxistas, trenes y Sheldon"  
date: 2019-01-24  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/06.TaxistasTrenesSheldon/06.TaxistasTrenesSheldon  
tags: [ Taxistas, Trenes, Sheldon, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento sobre los taxistas, trenes y que a veces me parezco a Sheldon.  


<audio controls>
  <source src="https://archive.org/download/06.TaxistasTrenesSheldon/06.TaxistasTrenesSheldon.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>
