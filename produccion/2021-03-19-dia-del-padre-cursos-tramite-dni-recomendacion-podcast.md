---
layout: post  
title: "PPF-Dia del padre, cursos, aplicación tr@amite, dni y recomendación podcast"  
date: 2021-03-19  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/172.-cursos-tramite-recomendacion-podcast/172.CursosTramiteRecomendacionPodcast  
tags: [Dia del padre, Cursos, Dockers, Kubernetes, Alex Pro, DNI, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os felicito el día del padre, os hablo de cursos, la genial aplicación Tr@amite para android, os cuento alguna anécdota del dni y os recomiendo Desahogo Geek

Aplicación Tr@amite de CQe-Solutions

https://play.google.com/store/apps/dev?id=5450188462309874352


Desahogo Geek de Alex Pro

https://anchor.fm/s/1218850/podcast/rss  


<audio controls>
  <source src="https://archive.org/download/172.-cursos-tramite-recomendacion-podcast/172.CursosTramiteRecomendacionPodcast.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
