---
layout: post  
title: "PPF-Mi carpeta de podcast"  
date: 2021-10-29  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/205.-carpeta-podcast/205.CarpetaPodcast  
tags: [Podcast, Recomendaciones, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os hablo de mi carpeta de podcast en la que voy atesorando episodios que me han gustado y que guardado para oírlos mas adelante.  




<audio controls>
  <source src="https://archive.org/download/205.-carpeta-podcast/205.CarpetaPodcast.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
