---
layout: post  
title: "Microfono, seguridad en la red, reciclaje y duda windows"  
date: 2020-02-19  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/117.microfonoseguridadredreciclajeduda/117.MicrofonoSeguridadRedReciclajeDuda  
tags: [Europol, Auphonic, Anécdotas, DNI, JPOD, PHP, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento que un buen amigo me ha hecho llegar un micrófono nuevo, os recomiendo unas páginas de seguridad en la red , comento sobre reciclaje y dejo una duda sobre Windows en el aire.  


Referencias de hoy

+ Documental sobre reciclaje 
[https://www.youtube.com/watch?v=YRP1HiDT2Is](https://www.youtube.com/watch?v=YRP1HiDT2Is)

+ Canal de Youtube Papá Friki
[https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw](https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw)

+ Incibe 017
[https://www.incibe.es/linea-de-ayuda-en-ciberseguridad/](https://www.incibe.es/linea-de-ayuda-en-ciberseguridad)


+ @Pantallas_Amigas
[https://twitter.com/PantallasAmigas/](https://twitter.com/PantallasAmigas)

+ Test phising Google
[https://phishingquiz.withgoogle.com/?hl=es/](https://phishingquiz.withgoogle.com/?hl=es)


<audio controls>
  <source src="https://archive.org/download/117.microfonoseguridadredreciclajeduda/117.MicrofonoSeguridadRedReciclajeDuda.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
