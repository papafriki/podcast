---
layout: post  
title: "PPF-Poca cosa"  
date: 2022-03-02  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/223.-poca-cosa/223.PocaCosa  
tags: [ Irrelevante, Ucrania, Apple, Mcbook Air, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os recomiendo varios podcast y os traigo nuevas anécdotas del D.N.I.  



Podcast de la semana: HardwareAdictos -- Todo es irrelevante
<br>
[https://www.ivoox.com/todo-es-irrelevante-audios-mp3_rf_82795872_1.html](https://www.ivoox.com/todo-es-irrelevante-audios-mp3_rf_82795872_1.html)


<br>
<audio controls>
  <source src="https://archive.org/download/223.-poca-cosa/223.PocaCosa.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
