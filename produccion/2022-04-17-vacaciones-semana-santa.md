---
layout: post  
title: "PPF-Vacaciones Semana Santa"  
date: 2022-04-17  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/229.-vacaciones-seman-santa/229.VacacionesSemanSanta  
tags: [Vacaciones, Semana Santa, Deporte, Bicicletas, Lenovo, Thinplus LP80, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento nuestras vacaciones de Samana Santa.  



Podcast de la semana: Oculta. Intriga a flor de piel
<br>
[https://open.spotify.com/show/673guEp1MQ9yFQLRTFVJGN](https://open.spotify.com/show/673guEp1MQ9yFQLRTFVJGN)
<br>
<br>
Charlando con... un jardinero de campo de golf
<br>
[https://anchor.fm/alberto5757/episodes/CC-Jardinero-de-un-campo-de-golf-e1gnn21](https://anchor.fm/alberto5757/episodes/CC-Jardinero-de-un-campo-de-golf-e1gnn21)
<br>

<br>
<br>

<audio controls>
  <source src="https://archive.org/download/229.-vacaciones-seman-santa/229.VacacionesSemanSanta.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
