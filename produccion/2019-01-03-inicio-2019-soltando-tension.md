---
layout: post  
title: "Iniciando el 2019 y soltando tensiones"  
date: 2019-01-03  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/Inicio2019SoltandoTension/Inicio2019SoltandoTension  
tags: [ Pixel, eBay, Redmi Note 5, Papá Friki]  
comments: true 
---
Buenas muchachada, inicio el 2019 y os cuento cosas sobre el redmi note 5, el Pixel del suegro y os comparto un audio que grabé la semana pasada a la salida del trabajo en el que libero tensiones.  


<audio controls>
  <source src="https://archive.org/download/Inicio2019SoltandoTension/Inicio2019SoltandoTension.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>
