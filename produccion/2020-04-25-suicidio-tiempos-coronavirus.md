---
layout: post  
title: "Suicido en tiempos del coronavirus"  
date: 2020-04-25  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/126.-suicido-en-tiempos-del-coronavirus/126.SuicidoEnTiemposDelCoronavirus  
tags: [ Covid19, Coronavirus, Suicidio, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy recogiendo el guante que me lanzó en una charla @poliorcetes vuelvo a tocar un tema difícil en estos tiempos pero que sigue siendo necesario darle visibilidad, es el suicidio.  

Os dejo enlace a la guía de la O.M.S. que comento al final del episodio.

[https://www.who.int/mental_health/prevention/suicide/resource_responders_spanish.pdf](https://www.who.int/mental_health/prevention/suicide/resource_responders_spanish.pdf)


Y dejo enlaces a las noticias comentadas en el mismo:

+ [https://elcaso.elnacional.cat/es/sucesos/coronavirus-chica-19-anos-suicida-no-soporta-cuarentena-emily-owen_28989_102.html](https://elcaso.elnacional.cat/es/sucesos/coronavirus-chica-19-anos-suicida-no-soporta-cuarentena-emily-owen_28989_102.html)


+ [https://www.20minutos.es/noticia/4208182/0/enfermera-suicida-infectado-coronavirus-italia/](https://www.20minutos.es/noticia/4208182/0/enfermera-suicida-infectado-coronavirus-italia/)


+ [https://www.abc.es/internacional/abci-relacionan-suicidio-politico-aleman-crisis-coronavirus-202003292013_noticia.html](https://www.abc.es/internacional/abci-relacionan-suicidio-politico-aleman-crisis-coronavirus-202003292013_noticia.html)


+ [https://www.20minutos.es/videos/economia/4199521-hombre-amenaza-con-degollarse-en-tres-cantos/](https://www.20minutos.es/videos/economia/4199521-hombre-amenaza-con-degollarse-en-tres-cantos/)


+ [https://www.telemundo47.com/noticias/hombre-intenta-suicidio-por-policia-tras-supuesto-diagnostico-de-coronavirus/2038851/](https://www.telemundo47.com/noticias/hombre-intenta-suicidio-por-policia-tras-supuesto-diagnostico-de-coronavirus/2038851/)

<audio controls>
  <source src="https://archive.org/download/126.-suicido-en-tiempos-del-coronavirus/126.SuicidoEnTiemposDelCoronavirus.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
