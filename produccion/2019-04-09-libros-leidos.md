---
layout: post  
title: "Libros leidos y cosas varias"  
date: 2019-04-09  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/072.LibrosLeidos/072.LibrosLeidos  
tags: [ ios.org.es, Libros Leidos, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os comento los libros leídos tras el examen de febrero y os comento varias cosas más del día a día.  


<audio controls>
  <source src="https://archive.org/download/072.LibrosLeidos/072.LibrosLeidos.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:


+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>

