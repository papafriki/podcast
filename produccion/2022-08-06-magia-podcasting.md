---
layout: post  
title: "PPF-Magia del podcasting"  
date: 2022-08-06  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/245.-magia-podcasting/245.MagiaPodcasting  
tags: [Western Digital, NAS, Fallo,  Reunion, Podcasters, Papá Friki]  
comments: true 
---
Buenas muchacha hoy os hablo de la magia del podcasting, la comunidad que lo forma que es única.  

<br><br>
Podcast de la semana: Monos del espacio -- Un podcast dedicado internet eterna
<br><br>
[https://www.ivoox.com/mde-un-podcast-dedicado-internet-eterna-audios-mp3_rf_90581015_1.html](https://www.ivoox.com/mde-un-podcast-dedicado-internet-eterna-audios-mp3_rf_90581015_1.html)
<br><br>

Charlando con un operario de cadena de montaje
<br><br>
[https://anchor.fm/alberto5757/episodes/CC-Operario-de-cadena-de-montaje-e1m5ee2/a-a8bk9gh](https://anchor.fm/alberto5757/episodes/CC-Operario-de-cadena-de-montaje-e1m5ee2/a-a8bk9gh)
<br><br>


<br>
<audio controls>
  <source src="https://archive.org/download/245.-magia-podcasting/245.MagiaPodcasting.mp3" type="audio/mpeg">  
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>

