---
layout: post  
title: "PPF-Mi relación con el deporte"  
date: 2022-02-03  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/219.-deporte/219.Deporte  
tags: [Ciclismo, Fútbol, Natación, Deporte, Running, Ángel Martín, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre una devolucion en Amazon y anécdotas del dni.  


Aplicación Ejercicios en casa
<br>
[https://play.google.com/store/apps/details?id=homeworkout.homeworkouts.noequipment&hl=es&gl=US](https://play.google.com/store/apps/details?id=homeworkout.homeworkouts.noequipment&hl=es&gl=US)


<br>
<audio controls>
  <source src="https://archive.org/download/219.-deporte/219.Deporte.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>


Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/<br>
