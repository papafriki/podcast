---
layout: post  
title: "PPF-Coronavirus infantil y firefly III "  
date: 2021-04-08  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/175.-coronavirus-fire-fly-3/175.CoronavirusFireFly3  
tags: [Coronavirus Infantil, FireFlayIII, Va por los mares, Papá Friki]  
comments: true 
---
Buenas muchachada, perdonar la calidad del audio de hoy. Os comento sobre el Covid en niños y el docker de fireflay III.  


Va por los mares

[https://anchor.fm/s/4e719528/podcast/rss](https://anchor.fm/s/4e719528/podcast/rss)


<audio controls>
  <source src="https://archive.org/download/175.-coronavirus-fire-fly-3/175.CoronavirusFireFly3.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
