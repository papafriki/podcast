---
layout: post  
title: "Plan B"  
date: 2019-10-08  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/097.planb/097.PlanB  
tags: [Lucas, Plan B, Reality Bites, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy doy mi opinión a un episodio publicado por Lucas en su podcast Reality Bites y os invito a ir a la Maratonpod contando como fue mi primer evento de podcast.

[Reality Bites: https://heyazor.in](https://heyazor.in/tagged/podcast)

<audio controls>
  <source src="https://archive.org/download/097.planb/097.PlanB.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/)
