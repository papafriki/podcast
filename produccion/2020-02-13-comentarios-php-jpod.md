---
layout: post  
title: "Comentarios, Europol, varios y JPOD"  
date: 2020-02-13  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/116.comentarioseuropolanecdotasphp/116.ComentariosEuropolanecdotasPhp  
tags: [Europol, Auphonic, Anécdotas, DNI, JPOD, PHP, Papá Friki]  
comments: true 
---
Buenas muchachada hoy he metido de todo en el episodio.  


Referencias de hoy

+ Europol 
[https://www.europol.europa.eu/stopchildabuse](https://www.europol.europa.eu/stopchildabuse)

+ Canal de Youtube Papá Friki
[https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw](https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw)

+ Auphonic
[http://auphonic.com/](http://auphonic.com/)


<audio controls>
  <source src="https://archive.org/download/116.comentarioseuropolanecdotasphp/116.ComentariosEuropolanecdotasPhp.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
