---
layout: post  
title: "PPF-Devolución, saturado y anécdotas D.N.I."  
date: 2022-01-26  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/218.-devolucion-saturado-anecdotas/218.DevolucionSaturadoAnecdotas  
tags: [Bosch,Amazon, Cobra Kay, Duolingo, Anécdotas, D.N.I.,Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre una devolucion en Amazon y anécdotas del D.N.I.  


Podcast de la semana: Duolingo -- Aprender ingles

[https://www.ivoox.com/podcast-relatos-ingles-duolingo_sq_f1968604_1.html](https://www.ivoox.com/podcast-relatos-ingles-duolingo_sq_f1968604_1.html)


<br>
<audio controls>
  <source src="https://archive.org/download/218.-devolucion-saturado-anecdotas/218.DevolucionSaturadoAnecdotas.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>


Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/<br>
