---
layout: post  
title: "PPF-Variado de Semana Santa"  
date: 2021-03-30  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/174.-semana-santa/174.SemanaSanta  
tags: [Tr@amite, Diego Cid, Kubernetes, Docker, Coursera, Semana Santa, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento cómo hemos llegado a la Semana Santa y que estamos haciendo con los niños en casa.  


La webcam que comento y que Andrés Ramos recomendó
https://www.amazon.es/gp/product/B07R3KH1XS/

Aplicación Tr@amite de CQe-Solutions

https://play.google.com/store/apps/dev?id=5450188462309874352


<audio controls>
  <source src="https://archive.org/download/174.-semana-santa/174.SemanaSanta.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
