---
layout: post  
title: "PPF-Vuelta al cine y reflexionando sobre el mundo del podcasting"  
date: 2021-01-01  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/160.-experiencia-cine-reflexion-podcast/160.ExperienciaCineReflexionPodcast  
tags: [ Cine, Niños, Podcasting, Publicidad, Reflexiones, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento como ha sido la vuelta al cine con los niños y en la segunda parte hago una reflexión personal del mundo del podcasting y de la publicidad.  

Perdonar la calidad del audio y los golpes que hoy se oyen.

<audio controls>
  <source src="https://archive.org/download/160.-experiencia-cine-reflexion-podcast/160.ExperienciaCineReflexionPodcast.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
