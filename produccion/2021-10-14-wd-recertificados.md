---
layout: post  
title: "PPF-Recertificados WD, finde rural y anécdotas"  
date: 2021-10-14  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/203.-recertificados-wdrural-anecdotas/203.RecertificadosWDRuralAnecdotas  
tags: [Anécdotas, D.N.I., WD, Recertificado, Rural, Ebay, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento sobre un wd recertificado, el mundo rural y anécdotas.  


Podcast de la semana: Sumando Pocast
[https://www.ivoox.com/autosumando-calamares-audios-mp3_rf_76669937_1.html](https://www.ivoox.com/autosumando-calamares-audios-mp3_rf_76669937_1.html)


Pagina web de los discos recertificados de WD

[https://shop.westerndigital.com/es-es/c/recertified](https://shop.westerndigital.com/es-es/c/recertified)


<audio controls>
  <source src="https://archive.org/download/203.-recertificados-wdrural-anecdotas/203.RecertificadosWDRuralAnecdotas.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
