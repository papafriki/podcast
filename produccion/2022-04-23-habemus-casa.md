---
layout: post  
title: "PPF-Habemus casa!!!"  
date: 2022-04-23  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/230.-habemus-casa/230.HabemusCasa  
tags: [Casa, Atico, Compra, Suicidio, D.N.I., Detenido, Papá Friki]  
comments: true 
---
Buenas muchachada: Habemus casa!!! con anécdotas del D.N.I. y hablando sobre el suicidio.  


Podcast de la semana: El Mancuentro  -- Viernes y suicidios
<br>
[https://anchor.fm/mancuentro/episodes/Viernes-y-suicidios-e1hgpgl](https://anchor.fm/mancuentro/episodes/Viernes-y-suicidios-e1hgpgl)
<br>
<br>
Charlando con... un jardinero de campo de golf
<br>
[https://anchor.fm/alberto5757/episodes/CC-Jardinero-de-un-campo-de-golf-e1gnn21](https://anchor.fm/alberto5757/episodes/CC-Jardinero-de-un-campo-de-golf-e1gnn21)
<br>
<br>

<audio controls>
  <source src="https://archive.org/download/230.-habemus-casa/230.HabemusCasa.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
