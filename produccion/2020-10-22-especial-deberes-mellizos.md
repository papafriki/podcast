---
layout: post  
title: "PPF-Especial deberes con los mellizos"  
date: 2020-10-22  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/149.-especial-deberes-mellizos/149.EspecialDeberesMellizos  
tags: [Mellizos, Hector, Leire, Deberes, Familia, Primaria, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os traigo un episodio diferente ya que aprovechando los deberes de Hector y Leire hacemos un audio que compartimos con vosotros.  

Skill para oir Sumando Podcast

[https://www.amazon.es/dp/B08LNY872W/?ref-suffix=ss_copy](https://www.amazon.es/dp/B08LNY872W/?ref-suffix=ss_copy)

<audio controls>
  <source src="https://archive.org/download/149.-especial-deberes-mellizos/149.EspecialDeberesMellizos.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)


