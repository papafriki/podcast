---
layout: post  
title: "Moviles en adolescentes"  
date: 2018-08-09  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/034.MovilesAdolescentes/034.MovilesAdolescentes  
tags: [ Adolescentes, Aplicaciones, Móviles, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os hago una breve reflexión sobre los móviles y los adolescentes tras haber leído un par de noticias al respecto.  

Referencias del episodio de hoy:   
+ [Francia prohibe por completo el uso de moviles en las escuelas](https://www.teknofilo.com/francia-prohibe-por-completo-el-uso-de-smartphones-en-los-colegios/)  
+ [Aplicaciones que no debe tener un adolescente en el móvil](https://www.educaciontrespuntocero.com/noticias/aplicaciones-no-recomendadas-para-ninos/86499.html)


<audio controls>
  <source src="https://archive.org/download/034.MovilesAdolescentes/034.MovilesAdolescentes.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
