---
layout: post  
title: "PPF-Sin preguntas pero con anécdotas "  
date: 2021-10-08  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/202.-sin-preguntas-anecdotas/202.SinPreguntasAnecdotas  
tags: [Anécdotas, D.N.I., Papá Friki]  
comments: true 
---
Buenas muchachada hoy no os respondo ninguna pregunta pero os cuento alguna anécdota.  


<audio controls>
  <source src="https://archive.org/download/202.-sin-preguntas-anecdotas/202.SinPreguntasAnecdotas.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
