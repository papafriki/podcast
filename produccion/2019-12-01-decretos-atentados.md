---
layout: post  
title: "Decretos y atentados"  
date: 2019-12-01  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/105.decretosyatentandos/105.DecretosyAtentandos  
tags: [One Plus, Pixel 2 xl, Running, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy me salgo un poco de la temática habitual y os comento el decretazo del gobierno y os doy mi punto de vista sobre los atentados en Londres y La haya.  

Texto del Decreto Ley 14/2019
[http://noticias.juridicas.com/base_datos/Admin/653286-real-decreto-ley-14-2019-de-31-de-octubre-por-el-que-se-adoptan-medidas-urgentes.html](http://noticias.juridicas.com/base_datos/Admin/653286-real-decreto-ley-14-2019-de-31-de-octubre-por-el-que-se-adoptan-medidas-urgentes.html)

El decretazo digital tambien va contra tí
[cgtinformatica.org/es/contenido/el-decretazo-digital-tambien-va-contra-ti](cgtinformatica.org/es/contenido/el-decretazo-digital-tambien-va-contra-ti)

Decretos Ley por presidentes en sus primeros meses de legistlatura
[https://www.elconfidencial.com/espana/2019-03-02/decretazos-pedro-sanchez-record-rajoy-aznar_1857862/](https://www.elconfidencial.com/espana/2019-03-02/decretazos-pedro-sanchez-record-rajoy-aznar_1857862/)


<audio controls>
  <source src="https://archive.org/download/105.decretosyatentandos/105.DecretosyAtentandos.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
