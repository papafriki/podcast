---
layout: post  
title: "PPF-Sin Podcast"  
date: 2021-12-29  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/214.-sin-podcast/214.SinPodcast
tags: [Papá Friki]  
comments: true 
---
Por motivos ajenos al podcaster con carnet esta semana no habrá episodio.  

<br>
<audio controls>
  <source src="https://archive.org/download/214.-sin-podcast/214.SinPodcast.mp3" type="audio/mpeg">
  </audio>
  <br>
  Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
  <br>
  https://feedpress.me/sospechososhabituales
  <br>
  Os recuerdo, los métodos de contacto son:
  <br>
  + Web: <http://www.papafriki.es><br>
  + Twitter: <https://twitter.com/papa_friki><br>
  + Correo: <papafrikipodcast@gmail.com><br>
  + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
  + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
  + Feed itunes: <https://itunes.apple.com/es/podcast/
