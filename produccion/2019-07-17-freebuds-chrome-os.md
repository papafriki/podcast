---
layout: post  
title: "Freebuds y Chrome OS"  
date: 2019-07-17  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/086.freebudschromebook/086.FreebudsChromebook  
tags: [ Freebuds, Chrome OS, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento mi experiencia de uso con los Huawei Freebuds 2 pro y como estoy intentando instalar en el Macbook pro un usb con Chrome OS.  


<audio controls>
  <source src="https://archive.org/download/086.freebudschromebook/086.FreebudsChromebook.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/)
