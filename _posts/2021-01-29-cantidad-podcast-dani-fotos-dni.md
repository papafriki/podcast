---
layout: post  
title: "PPF-Cantidad de podcast, gracias Dani y sobre fotos del D.N.I."  
date: 2021-01-29  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/164.-podcast-asaco-dani-fotos-dni/164.PodcastASacoDaniFotosDNI  
tags: [ Podcast, Dani, Wintablet, DNI, Antonio Manfredi, Sumando Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento los podcast en que he participado, le doy nuevamente las gracias a Dani un oyente que me ha echado una mano y os cuento más cosas sobre las fotos del D.N.I.  


El directorio de la Asociación Podcast
[https://asociacionpodcast.es/directorio/](https://asociacionpodcast.es/directorio/)


Si te registras y usas el código AG2Q02 tendrás 5€ de descuento el primer año.
[https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02](https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02)

<audio controls>
  <source src="https://archive.org/download/164.-podcast-asaco-dani-fotos-dni/164.PodcastASacoDaniFotosDNI.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
