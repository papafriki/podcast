---
layout: post  
title: "PPF-One Plus 6T con android 11, mifi y anecdotas del D.N.I."  
date: 2021-07-14  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/189.-one-plus/189.OnePlus  
tags: [One Plus, 6T, Androd 11, Beta, Mifi, Anécdotas, Mr. Robot, El Canon, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre la beta de Android 11 en el One Plus 6T y más anécdotas en el D.N.I.  


Podcast de la semana: [El Canon](https://www.ivoox.com/podcast-canon_fg_f1623449_filtro_1.xml)


[https://www.ivoox.com/084-macroresena-iphone-12-audios-mp3_rf_72565909_1.html](https://www.ivoox.com/084-macroresena-iphone-12-audios-mp3_rf_72565909_1.html)


<audio controls>
  <source src="https://archive.org/download/189.-one-plus/189.OnePlus.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
