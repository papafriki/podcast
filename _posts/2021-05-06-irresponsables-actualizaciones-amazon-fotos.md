---
layout: post  
title: "PPF-Irresponsables, actualizaciones y Amazon Fotos"  
date: 2021-05-06  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/180.-irresponsables-actaulizaciones-amazon-fotos/180.IrresponsablesActaulizacionesAmazonFotos  
tags: [ Covid, Amazon Fotos, OMV, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre irresponsables, actualizaciones fallidas y mi inicio con el uso de Amazon Fotos.  


<audio controls>
  <source src="https://archive.org/download/180.-irresponsables-actaulizaciones-amazon-fotos/180.IrresponsablesActaulizacionesAmazonFotos.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
