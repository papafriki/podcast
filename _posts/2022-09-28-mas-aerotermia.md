---
layout: post  
title: "PPF-Explicando más la aertoermia"  
date: 2022-09-28  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/253.-mas-aerotermia/253.MasAerotermia  
tags: [Aerotermia, Suelo radiante, Energías renovables, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os explico más la aerotermia y comento la primera factura que nos ha llegado.  


<br>
<audio controls>
  <source src="https://archive.org/download/253.-mas-aerotermia/253.MasAerotermia.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>

