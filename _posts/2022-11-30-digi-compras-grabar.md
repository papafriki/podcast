---
layout: post  
title: "PPF-Digi, compras y animaros a grabar"  
date: 2022-11-30  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/262.-compras-digi-grabar/262.ComprasDigiGrabar  
tags: [ Digi, Compras, Black Friday, Impresora 3D, 3D, CR-6, Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada hoy pediros perdón por la calidad del audio que no es la mejor y luego comentaros una llamada con Digi, compras del Black Friday y os animo a grabar.  


<br><br>
Charlando con... un actor de teatro 
[https://spotifyanchor-web.app.link/e/k9j9bD5m1ub](https://spotifyanchor-web.app.link/e/k9j9bD5m1ub)
<br><br>
Los últimos de feedlipinas -- Los Podcast dan felicidad a la gente 
[https://www.ivoox.com/podcast-dan-felicidad-a-gente-audios-mp3_rf_97046126_1.html](https://www.ivoox.com/podcast-dan-felicidad-a-gente-audios-mp3_rf_97046126_1.html)
<br>
<audio controls>
  <source src="https://archive.org/download/262.-compras-digi-grabar/262.ComprasDigiGrabar.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>



