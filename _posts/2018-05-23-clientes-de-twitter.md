---
layout: post  
title: "Clientes de Twitter"  
date: 2018-05-23  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/018.ClientesDeTwitter/018.ClientesDeTwitter  
tags: [Twitter, Plume, Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada, en el episodio de hoy os cuento mi experiencia volviendo a usar el cliente oficial de Twitter tras mas de un año usando Plume.  

![](https://i.imgur.com/Tej2WOh.jpg)


Referencias del episodio de hoy:   
+ [El Podcast Geek de Neroncete](https://play.google.com/store/apps/details?id=com.levelup.touiteur&hl=es)  
+ [Aplicación Plume for Twitter](https://play.google.com/store/apps/details?id=com.levelup.touiteur&hl=es)  
+ [Aplicación Twitter](https://play.google.com/store/apps/details?id=com.twitter.android)  

<audio controls>
  <source src="https://archive.org/download/018.ClientesDeTwitter/018.ClientesDeTwitter.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>