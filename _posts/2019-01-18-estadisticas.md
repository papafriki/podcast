---
layout: post  
title: "Estadísticas"  
date: 2019-01-18  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/060.Estadisticas/060.Estadisticas  
tags: [ Estadísticas, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy demuestro que no es bueno grabar muy pronto por la mañana y de paso os comento las estadísticas del podcast.  


<audio controls>
  <source src="https://archive.org/download/060.Estadisticas/060.Estadisticas.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>