---
layout: post  
title: "PPF-Problemas con Pihole en el NUC y voces sintéticas"  
date: 2020-11-26  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/154.-nuc-voces-sinteticas/154.NucVocesSinteticas  
tags: [ NUC, Linux, DNS, Pihole, Adguard, Voces sintéticas, Dislexia, Dictado diferido, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento algunos problemillas que estoy teniendo con el NUC, la pihole y la resolución de DNS.  


Además os traigo unas voces sintéticas de las que aquí os dejo los enlaces

[https://www.readspeaker.com/es/](https://www.readspeaker.com/es/)

[https://responsivevoice.org/text-to-speech-languages/texto-a-voz-en-espanol/](https://responsivevoice.org/text-to-speech-languages/texto-a-voz-en-espanol/)

[https://www.naturalreaders.com/online/](https://www.naturalreaders.com/online/)

[https://blog.changedyslexia.org/lectores-de-texto-gratuitos/](https://blog.changedyslexia.org/lectores-de-texto-gratuitos/)

[https://chrome.google.com/webstore/detail/read-aloud-a-text-to-spee/hdhinadidafjejdhmfkjgnolgimiaplp?hl=es](https://chrome.google.com/webstore/detail/read-aloud-a-text-to-spee/hdhinadidafjejdhmfkjgnolgimiaplp?hl=es)

Y aquí explican mejor que lo he hecho yo el dictado diferido.

[http://dislexiaydiscalculia.com/alternativa-al-dictado-la-copia-diferida/](http://dislexiaydiscalculia.com/alternativa-al-dictado-la-copia-diferida/)

<audio controls>
  <source src="https://archive.org/download/154.-nuc-voces-sinteticas/154.NucVocesSinteticas.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
