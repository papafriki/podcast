---
layout: post  
title: "Mamá Friki"  
date: 2018-08-19  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/035.MamaFriki/035.MamaFriki  
tags: [Mamá Friki]  
comments: true 
---
Buenas muchachada, mamá friki al habla.  




<audio controls>
  <source src="https://archive.org/download/035.MamaFriki/035.MamaFriki.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
