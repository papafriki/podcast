---
layout: post  
title: "Feliz Dia del papa, friki o no"  
date: 2018-03-19  
categories: podcast
image: images/dia-del-padre.jpg  
podcast_link: https://archive.org/download/004.DiaPapa/004.DiaPapa
tags: [podcast,alterrnativeto.net,dia del padre, Papa Friki]  
comments: true 
---
Feliz día del padre, y feliz santo a los Josés y Pepes, así como Josefas o Pepas que escuchéis el Podcast.  

Aquí os reseño la página web que he comentado hoy [https://alternativeto.net](https://alternativeto.net)

Ya sabéis,  nos escuchamos, leemos o vemos, hasta luego!


<audio controls>
  <source src="https://archive.org/download/004.DiaPapa/004.DiaPapa.mp3" type="audio/mpeg">
</audio>


![](https://papafriki.gitlab.io/podcast/images/dia-del-padre.jpg)  

Y recuerda, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
