---
layout: post  
title: "PPF-Hacienda, despistado que es uno y series"  
date: 2021-05-12  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/181.-hacienda-despiste-series/181.HaciendaDespisteSeries  
tags: [ App Store, Mac, iOS, Diego Cid Merino, Despiste, Series, Anécdotas, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre Hacienda, lo despistado que soy y las últimas series que he visto.  


Apliaciones en App Store de Diego Cid Merino:

[https://apps.apple.com/gb/developer/diego-cid-merino/id156470062](https://apps.apple.com/gb/developer/diego-cid-merino/id156470062)


<audio controls>
  <source src="https://archive.org/download/181.-hacienda-despiste-series/181.HaciendaDespisteSeries.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
