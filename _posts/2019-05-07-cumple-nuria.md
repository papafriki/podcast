---
layout: post  
title: "Cumpleaños Nuria"  
date: 2019-05-07  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/076.CumpleaosNuria/076.CumpleNuria  
tags: [ Cumpleaños, Nuria, Papá Friki]  
comments: true 
---
Buenas muchachada, el de 5 mayo ha sido el cumpleaños de Nuria y os cuento un poco como llegó a este mundo.  


<audio controls>
  <source src="https://archive.org/download/076.CumpleaosNuria/076.CumpleNuria.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot [https://swoot.com/papafriki](https://swoot.com/papafriki)