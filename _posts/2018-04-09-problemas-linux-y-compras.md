---
layout: post  
title: "Problemas con linux y compras"  
date: 2018-04-09  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/010.ProblemasLinuxCompras/010-Problemas-Linux-compras  
tags: [Linux,Xiaomi,Compras,Podcast,Papa Friki]  
comments: true 
---
Buenas muchachada, en el episodio de hoy os cuento unos problemas que he tenido con Linux y dos pequeñas compras que he hecho estos últimos días.  

![](https://papafriki.gitlab.io/podcast/images/logo.png)  

Os dejo enlace a los productos comentados hoy  (enlace de referidos de Frikismo Puro en los auriculares, se lo merecen):   
+ [Cascos bluetooth](https://www.amazon.es/Auriculares-In%C3%A1lambricos-ZOETOUCH-Magn%C3%A9ticos-Impermeables/dp/B075Q5B3W9/ref=sr_1_1?ie=UTF8&qid=1523304811&sr=8-1&keywords=B075Q5B3W9)  
+ [Mochila de Xiaomi](https://www.gearbest.com/laptop-bags/pp_357033.html)  

<audio controls>
  <source src="https://archive.org/download/010.ProblemasLinuxCompras/010-Problemas-Linux-compras.mp3" type="audio/mpeg">
</audio>

Y recuerda, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
