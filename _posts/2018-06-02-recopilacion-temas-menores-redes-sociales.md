---
layout: post  
title: "Recopilación de temas y menores en redes sociales"  
date: 2018-06-02  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/020.RecopilandoTemasYMenoresEnRedesSociales/020.Recopilandotemasymenoresenredessociales  
tags: [Twitter, Citroen, Papá Friki, Podcast]  
comments: true 
---
Buenas muchachada, en el episodio de hoy (¡el número 20 ya!) os comento y cierro unos temas anteriores (facturas Orange y averia del coche) y os comento mi visión de la privacidad y las fotografías de menores en las redes sociales.  


<audio controls>
  <source src="https://archive.org/download/020.RecopilandoTemasYMenoresEnRedesSociales/020.Recopilandotemasymenoresenredessociales.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>