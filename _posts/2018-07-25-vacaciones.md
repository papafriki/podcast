---
layout: post  
title: "Vacaciones"  
date: 2018-07-25  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/030.Vacaciones/030.Vacaciones  
tags: [ Vacas, Vacaciones, MazinguerAstur, Papá Friki]  
comments: true 
---
Buenas muchachada, os cuento que ya estamos de vacaciones, que Lowi no tiene cobertura en el pueblo en el que estamos y mi maleta tecnológica para estas vacaciones.  

![](https://i.imgur.com/PtJTuSU.jpg)

Referencias del episodio de hoy:   
+ [MazinguerAstur Podcast](http://feeds.feedburner.com/Mazingerastur)
+ [Androytecno](http://androytecno.com/)
+ [t.me/PodcastAndroytecno](t.me/PodcastAndroytecno)


<audio controls>
  <source src="https://archive.org/download/030.Vacaciones/030.Vacaciones.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
