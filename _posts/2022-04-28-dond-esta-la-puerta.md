---
layout: post  
title: "PPF-¿Dónde esta la puerta?"  
date: 2022-04-28   
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/231.-donde-esta-la-puerta/231.DondeEstaLaPuerta  
tags: [Casa, Atico, Depresión, Adriano, Puerta,Escaleras, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento como ha ido esta semana desde que tenemos la nueva casa.  



Podcast de la semana:  Loco al micro (Adriano) -- Depresión
<br>
[https://www.ivoox.com/lc-depresion-129394-conociendo-puedes-ayudar-mucho-audios-mp3_rf_86124892_1.html](https://www.ivoox.com/lc-depresion-129394-conociendo-puedes-ayudar-mucho-audios-mp3_rf_86124892_1.html)



<br>
<audio controls>
  <source src="https://archive.org/download/231.-donde-esta-la-puerta/231.DondeEstaLaPuerta.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
