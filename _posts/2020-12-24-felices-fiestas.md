---
layout: post  
title: "PPF-Felices fiestas"  
date: 2020-12-24  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/159.-felices-fiestas/159.FelicesFiestas  
tags: [ Felices Fiestas, Personal, Amazon, Sumando Pocast, Sospechosos habituales, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os felicito las fiestas y hago un resumen muy rápido del año.  

Podcast **Lo q sabes** de Alberto Simon

https://anchor.fm/s/6eb65bc/podcast/rss

<audio controls>
  <source src="https://archive.org/download/159.-felices-fiestas/159.FelicesFiestas.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
