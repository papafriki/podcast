---
layout: post  
title: "Vacaciones agridulces"  
date: 2018-05-08  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/015.VacacionesAgridulces/VacacionesAgridulces  
tags: [Valenica, Guadarrama, podcast, papa friki]  
comments: true 
---
Buenas muchachada, hoy tras un par de semanas sin grabar os cuento mis vacaciones por Valencia, Guadarrama y un problema que he tenido con el coche.  

![](https://i.imgur.com/nlu70TH.jpg)
![](https://i.imgur.com/CZ7b9Fy.jpg)


<audio controls>
  <source src="https://archive.org/download/015.VacacionesAgridulces/VacacionesAgridulces.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
