---
layout: post  
title: "PPF-Kindle, nuevos proyectos y anécdotas del D.N.I."  
date: 2022-02-09  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/220.-nuevos-proyectos-anecdotas-dni/220.NuevosProyectosAnecdotasDNI  
tags: [Kindle, Amazon, Podcast, Umlando, El oscuro pasajero, Anécdotas, D.N.I., Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre un problemilla con el kindle que usa Nuria, nuevos proyectos que empiezan y más anécdotas del D.N.I.  


Imanol -- Umlando
<br>
[https://anchor.fm/s/7fe96d4c/podcast/rss](https://anchor.fm/s/7fe96d4c/podcast/rss)
<br>
El oscuro pasajero de Félix
<br>
[https://anchor.fm/s/800d4578/podcast/rss](https://anchor.fm/s/800d4578/podcast/rss)


<br>
<audio controls>
  <source src="https://archive.org/download/220.-nuevos-proyectos-anecdotas-dni/220.NuevosProyectosAnecdotasDNI.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>


Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/<br>
