---
layout: post  
title: "Respuestas"  
date: 2018-09-20  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/041.Respuestas/041.Respuestas  
tags: [ Educando Geek, Monos del Espacio, Sr Lobo, Papá Friki]  
comments: true 
---
Buenas muchachada hoy doy la réplica a JGurillo de eDucando Geek, Monos del Espacio y las gracias al señor Lobo por el audio de sus peques.  


Referencias del episodio de hoy:

+ [eDucando Geek](https://educandogeek.github.io/)
+ [Monos del Espacio](http://www.ivoox.com/p_sq_f1148042_1.html)
+ [Sr Lobo](https://pca.st/6Ymq)

<audio controls>
  <source src="https://archive.org/download/041.Respuestas/041.Respuestas.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>