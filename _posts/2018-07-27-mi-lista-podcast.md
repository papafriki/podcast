---
layout: post  
title: "Mi lista de podcast"  
date: 2018-07-27  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/031.MiListaDePodcast/031.MiListaDePodcast  
tags: [ Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada, acudiendo a la llamada de @MonosdelEspacio en que nos invita a compartir los podcast que escuchamos, os comento en el episodio de hoy los míos.  

![](https://i.imgur.com/masVYWP.jpg)

Referencias del episodio de hoy:   
+ [Mi lista de podcast en Pocket Casts](https://lists.pocketcasts.com/f95621b4-adc4-49b9-9b9f-69376d09e3f1)

Os pongo tambien el enlace en bruto para que lo podais ver bien en todos los gestores podcast  

https://lists.pocketcasts.com/f95621b4-adc4-49b9-9b9f-69376d09e3f1

<audio controls>
  <source src="https://archive.org/download/031.MiListaDePodcast/031.MiListaDePodcast.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
