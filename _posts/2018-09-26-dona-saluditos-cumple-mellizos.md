---
layout: post  
title: "Dona sangre, saluditos y cumpleaños mellizos"  
date: 2018-09-26  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/042.DonaSangreSaluditosCumpleaosFeliz/042.DonaSangreSaluditosCumpleFeliz  
tags: [ Dona Sangre, Saluditos, Cumpleaños Mellizos, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os invito a donar sangre, mandamos saluditos a Erika y Leo y Google les canta el cumpleaños feliz a Leire y Hector.  


Referencias del episodio de hoy:

+ [Va por nosotras](http://www.ivoox.com/28028431)
+ [Sr Lobo](https://pca.st/6Ymq)

<audio controls>
  <source src="https://archive.org/download/042.DonaSangreSaluditosCumpleaosFeliz/042.DonaSangreSaluditosCumpleFeliz.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
