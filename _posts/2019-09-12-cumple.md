---
layout: post  
title: "Cumpleaños"  
date: 2019-09-12  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/093.cumple/093.Cumple  
tags: [  Cumpleaños, Monitor, Smartwatch, Huawei, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento los regalos tecnológicos que he tenido este año y os recomiendo un podcast de Reality Bites.

[Reme, 50 (Realiti bites)](https://anchor.fm/heyazorin/episodes/HEY-74--Reme--50-e4tb9j/a-amd4rl) 

[Suicidio (Papá Friki)](https://papafriki.gitlab.io/podcast/suicidio/) 


<audio controls>
  <source src="https://archive.org/download/093.cumple/093.Cumple.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/)
