---
layout: post  
title: "Transcripción: Jaime Javier Aguilar Brites"  
date: 2021-04-20  
categories: post  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
tags: [Jaime Javier Aguilar Brites, Historias, Papá Friki]  
comments: true 
---
Bienvenido al podcast de Papá Friki, soy Alberto. 

¿Eres capaz de recordar que hacías el 13 de julio de 2006? Seguramente no, pero si te pregunto que hacías el 11 septiembre de 2001, o el 11 marzo de 2004 que ha pasado más tiempo desde aquellas fechas seguro que si eres capaz de recordarlo.... Pero hoy esas otras fechas no son las importantes, hoy sólo nos centraremos en la fecha del 13 de julio 2006.


El protagonista de ésta historia se llama Jaime Javier Aguilar Brites, paraguayo para más señas y que en esos momentos tiene 38 años. Como otros muchos compatriotas suyos se encuentra de forma ilegal en España. Ha conseguido traer a su mujer a España hace poco menos de 7 meses, es una sensación agridulce, ya que su mujer está con él pero en su tierra natal siguen sus tres hijos viviendo con familiares suyos y a los que mandan todo el dinero que pueden.

Al poco de llegar consiguió encontrar un trabajo para la empresa Carrocerías Tomás SL. En esas fechas están como subcontrata de la empresa Telegest SL, concretamente realizan unos trabajos de acondicionamiento y reforma de instalaciones en los sótanos de la sede del Partido Popular, en la calle Génova número 13 de Madrid.

Como estaba haciendo los días anteriores, el día 13 entró por la parte de atrás del edificio usando los garajes para no tener que ser identificado por el personal de seguridad al no tener documentación y no haber sido legalizado por la empresa.

Cuándo iba a comenzar a realizar una soldadura y, al encender el equipo, se produjo una fuerte deflagración por los gases acumulados en el interior de un depósito de agua. Jaime Javier se vio envuelto en llamas y su compañero de trabajo, que también sufrió quemaduras en las manos, pudo a duras penas arrastrar a Jaime al exterior.

Ahí dió comienzo su infierno, del accidente no recuerda nada. Fue trasladado a urgencias del hospital de La Paz, con quemaduras del 85% de su cuerpo y con síndrome de inhalación, precisó repetidas intervenciones para poder salvar su vida. 

Al final fueron 11 las operaciones y 7 los meses que Jaime Javier estuvo ingresado. Trasplantes de piel, de músculos, amputación de un dedo y una falange, implante de uñas... y dolores, muchos dolores. 

Cuenta, que mientras estuvo debatiéndose entre la vida y la muerte los primeros días tras el accidente soñó que la casa de su madre, dónde estaban viviendo sus tres hijos y algunos de sus nueve hermanos en Paraguay ardía en las llamas. Cuándo se despertó lo comentó con su mujer y le explicaron que no había sido un sueño, que realmente así había ocurrido y su familia había tenido que estar viviendo en casa de otros familiares y vecinos. Nuevamente el mundo se volvia a venir abajo.


Durante todo ese tiempo la empresa que le tenía contratado (lo diremos así, pero como ya se ha dicho, lo tenían sin contrato dado de alta en la seguridad social por estar sin papeles en España) le dejó en la estacada y se desentendió de él y de su mujer, por lo que tuvo que subsistir gracias a la caridad de amigos, vecinos y personal del hospital en un piso compartido del barrio de Pacífico. 

15 meses después del accidente, el Juzgado de lo Social número 12 de Madrid reconoce su relación laboral con las empresas que le tenían contratado y subcontratado, pero deja al margen al de la responsabilidad al PP. Aunque también extiende la responsabilidad a la mutua Fremap como aseguradora de las anteriores. 


Las obliga a abonar el subsidio de incapacidad laboral por su accidente, 36 euros diarios, desde julio de 2006 hasta que le dieran el alta o la incapacidad permanente, cosa ésta que nunca llegó a ocurrir y al final recibió su indemnización por el tiempo que estuvo de baja. 

Mucha gente hubiera recurrido la incapacidad permanente, se han visto casos mucho más leves que la han conseguido pero Jaime Javier es un trabajador, no sabe hacer otra cosa y quiere seguir haciéndolo.


En 2008 Mercedes Milá se interesó por su caso y en marzo se emitió un programa de "Diario de..." dedicado a la explotación laboral a inmigrantes, por entonces no se usaba la palabra migrantes. 

Gracias a esa intermediación logró conseguir traer a sus tres hijos con él. Por medio del sindicato UGT, que le puso el abogado en su momento, su mujer consiguió un empleo y poco a poco han podido rehacer su vida. 


Y todo ésto os lo cuento porque Jaime Javier tenía que renovarse los certificados del dni el otro día y vino a mi oficina a realizar el trámite, que lógicamente fue un tramite complicado de hacer en el kiosko al no tener huellas dactilares. Finalmente tuvimos que expedirle otra vez un nuevo dni y volver a tomar huellas. 

Las huellas son necesarias para poder expedir el certificado digital, si no se pueden tomar no se genera certificado electrónico y en este caso, Jaime Javier, si lo necesita para seguir cumpliendo sus obligaciones para con el Estado.


Y si os lo preguntáis ya os lo confirmo yo, venía de trabajar, con su ropa de trabajo, recién salido de una obra cercana en la que sigue trabajando, ahora sí, con un contrato y pasando días mejores y otros peores, porque según me dijo duele, duele si cambia el tiempo, si hay humedad o si está más seco el tiempo. 

Me contó, que al salir en la prensa se había hecho famoso en su tierra cuando ocurrio el accidente, a lo que le comenté que mejor no haber salido del anonimato por ése motivo. Su respuesta me dejó impresionado: "mejor por ésto que por hacer cosas malas o daño a otras personas". 



Esta historia es una producción de Papá Friki 

Fue producida y escrita por Papá Friki, basándome en reportajes de diversos periódicos y la charla mantenida con Jaime Javier.

La edición es de Papá Friki. 

El diseño de sonido es de Papá Friki.

Papá Friki es el CEO de Papá Friki. 

El episodio ha sido producido y se mezcla en el programa Audacity. 

Gracias por escuchar.

 
Papá Friki pertenece a la Red de sospechosos habituales, puedes seguir la Red y a todos sus integrantes mediante el feed: 

[feedpress.me/sospechososhabituales](feedpress.me/sospechososhabituales )


Un saludo, nos vemos, nos leemos, nos escuchamos, adiós 

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
