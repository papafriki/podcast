---
layout: post  
title: "PPF-Anécdotas dni y serie"  
date: 2020-07-07  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/135.-anecdotas-dni-series/135.AnecdotasDniSeries  
tags: [  Monedas, DNI, Series, Dark, Frikimo Puro, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento algunas anecdotas del D.N.I. y os recomiendo una serie.  

Os enlazo el artículo 11 del Reglamento (CE) nº 2169/2005 del Consejo, de 21 de diciembre de 2005, por el que se modifica el Reglamento (CE) nº 974/98 sobre la introducción del euro y en el que podeis ver la limitación de las 50 monedas.

[https://www.boe.es/buscar/doc.php?id=DOUE-L-2005-82620](https://www.boe.es/buscar/doc.php?id=DOUE-L-2005-82620)

La pagina para poder seguir Dark

[https://darknetflix.io/es](https://darknetflix.io/es)

<audio controls>
  <source src="https://archive.org/download/135.-anecdotas-dni-series/135.AnecdotasDniSeries.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
