---
layout: post  
title: "Brexit y DNI 3.0"  
date: 2020-01-29  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/114.brexitdnie3/114.BrexitDnie3  
tags: [ Brexit, DNI, Anécdotas, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os comento cosas que ya se van sabiendo del BREXIT y nuevas formas de usar el DNI 3.0  


Nota del Ministerio de Asuntos Exteriores

[http://www.exteriores.gob.es/Embajadas/LONDRES/es/VivirEn/Paginas/DocumentacionTramites.aspx](http://www.exteriores.gob.es/Embajadas/LONDRES/es/VivirEn/Paginas/DocumentacionTramites.aspx)

Para llamadas desde el Reino Unido: +44 (0) 1158 575508

Para llamadas desde España: +34 91 8362248

Descargas del DNI 

[https://www.dnielectronico.es/PortalDNIe/PRF1_Cons02.action?pag=REF_1100](https://www.dnielectronico.es/PortalDNIe/PRF1_Cons02.action?pag=REF_1100)

Aplicación Dnie Smart connect

[Dnie Smart Connect](https://play.google.com/store/apps/details?id=es.gob.fnmt.dniesmartconnect)


<audio controls>
  <source src="https://archive.org/download/114.brexitdnie3/114.BrexitDnie3.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)