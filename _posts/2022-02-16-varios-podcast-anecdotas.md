---
layout: post  
title: "PPF-Varios podcast y anécdotas del D.N.I."  
date: 2022-02-16  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/221.-varios-podcast-anecdotas-dni/221.VariosPodcastAnecdotasDNI  
tags: [Desde el reloj, Manías, Llaves, D.N.I., Anécdotas, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os recomiendo varios podcast y os traigo nuevas anécdotas del D.N.I.  


Felicidades al podcast Desde el reloj  por sus 300 episodios.
<br>
[https://www.ivoox.com/podcast-desde-reloj_sq_f11070086_1.html](https://www.ivoox.com/podcast-desde-reloj_sq_f11070086_1.html)
<br>
<br>

Podcast de la semana: Gabinete de curiosidades -- T4 Episodio 4 El nuevo guiñol
<br>
[https://www.gabinetepodcast.com/t4-e4](https://www.gabinetepodcast.com/t4-e4)


<br>
<audio controls>
  <source src="https://archive.org/download/221.-varios-podcast-anecdotas-dni/221.VariosPodcastAnecdotasDNI.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
