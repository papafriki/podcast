---
layout: post  
title: "PPF-Sobre nases.Resubido"  
date: 2022-08-22  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/248.-sobre-nases/248.SobreNases  
tags: [WD, MyCloud, Gen 1, Asustor, Nimbustor, AS5304T, Papá Friki]  
comments: true 
---
Buenas muchachada hoy únicamente hablo sobre nases, el WD MyCloud Gen 1 y el Nimbustor de 4 bahías.  

<br><br>
Entrada explicando como restaura el WD MyCloud Gen 1
<br><br>
[https://papafriki.gitlab.io/podcast/recuperar-wd-mycloud-gen-1/](https://papafriki.gitlab.io/podcast/recuperar-wd-mycloud-gen-1/)
<br><br>
Se está votando la primera fase de los premios de la asociación podcast hasta el 31 de Agosto, si eres socio o simpatizante y te acuerdas de Charlando con... como podcast revelación se agradece tu voto.

[https://asociacionpodcast.es/votacion-inicial-premios-asociacion-podcast-edicion-2022/](https://asociacionpodcast.es/votacion-inicial-premios-asociacion-podcast-edicion-2022/)
<br><br>
Podcast de la semana: Cápsulas -- Pinball
<br><br>
[https://www.ivoox.com/395-pinball-audios-mp3_rf_90726973_1.html](https://www.ivoox.com/395-pinball-audios-mp3_rf_90726973_1.html)
<br><br>

<br>
<audio controls>
  <source src="https://archive.org/download/248.-sobre-nases/248.SobreNases.mp3" type="audio/mpeg">  
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>



