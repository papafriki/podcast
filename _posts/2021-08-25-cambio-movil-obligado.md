---
layout: post  
title: "PPF-Cambio de móvil obligado."  
date: 2021-08-25  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/196.-cambio-movil-obligado/196.CambioMovilObligado  
tags: [Movil, Huawei, P30, Home Assistant, Pixel 2 Xl, Google, Fotos, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento que he cambiado de móvil de forma obligada.  


Podcast de la semana: Hardwareadictos

https://www.ivoox.com/me-he-metido-home-assistant-ahora-no-audios-mp3_rf_74377818_1.html


Aplicación Photo Scan de Google

https://www.google.com/intl/es/photos/scan/


<audio controls>
  <source src="https://archive.org/download/196.-cambio-movil-obligado/196.CambioMovilObligado.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
