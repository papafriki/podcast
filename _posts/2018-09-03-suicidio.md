---
layout: post  
title: "Suicidio"  
date: 2018-09-03  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/039.Suicidio/039.Suicidio  
tags: [ Suicidio, Papá Friki]  
comments: true 
---
Buenas muchachada hoy traigo un tema complicado y diferente al que normalmente os traigo por aqui, que puede herir alguna sensibilidad.  


<audio controls>
  <source src="https://archive.org/download/039.Suicidio/039.Suicidio.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
