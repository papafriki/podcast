---
layout: post  
title: "Surface Pro y proyectos"  
date: 2020-02-06  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/115.surfaceproyectosnuevos/115.SurfaceProyectosNuevos  
tags: [ Surface Pro, Podmap, Proyectos, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento unas muy primeras impresiones sobre la Surface Pro 4 y os cuento en que proyectos ando metido.  


Podmap  

[https://podmap.org/location/ChIJwfLM7ACeeg0RXki8jh-gnY0](https://podmap.org/location/ChIJwfLM7ACeeg0RXki8jh-gnY0)

Canal Youtube Papá Friki

[https://podmap.org/location/ChIJwfLM7ACeeg0RXki8jh-gnY0](https://podmap.org/location/ChIJwfLM7ACeeg0RXki8jh-gnY0)

<audio controls>
  <source src="https://archive.org/download/115.surfaceproyectosnuevos/115.SurfaceProyectosNuevos.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)