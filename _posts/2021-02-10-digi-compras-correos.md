---
layout: post  
title: "PPF-Digi, últimas compras y problemas con Correos"  
date: 2021-02-10  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/166.-digi-compras-correos/166.DigiComprasCorreos  
tags: [ProximaStream, FileBot, OMV, Windows, Portable, Linux, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento como va el quitar el router de Digi, os explico las últimas compras y os cuento mi problema con un citypaq de Correos.  


Por cierto he dicho enchufe muchas veces y me referia al pulsador de la luz. El modelo exacto es el siguiente:

[https://es.aliexpress.com/item/4000191335997.html](https://es.aliexpress.com/item/4000191335997.html)

Los cascos que tenia:

[https://www.amazon.es/gp/product/B08NWZBHT1](https://www.amazon.es/gp/product/B08NWZBHT1)

El receptor que me he comprado es éste:

[https://www.amazon.es/gp/product/B07FVL4CN2](https://www.amazon.es/gp/product/B07FVL4CN2)

<audio controls>
  <source src="https://archive.org/download/166.-digi-compras-correos/166.DigiComprasCorreos.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
