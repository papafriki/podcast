---
layout: post  
title: "Parte, Rclone y anécdotas"  
date: 2019-10-04  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/096.partercloneanecdotasdni/096.ParteRcloneAnecdotasDNI  
tags: [Accidente, Rclone, Citroen C4, Anecdotas, Papá Friki]  
comments: true 
---
Buenas muchahada, hoy actualizo el parte de mi hermano, os comento sobre rclone y os cuento un par de anécdotas del dni.

<audio controls>
  <source src="https://archive.org/download/096.partercloneanecdotasdni/096.ParteRcloneAnecdotasDNI.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/)
