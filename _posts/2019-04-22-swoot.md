---
layout: post  
title: "Swoot"  
date: 2019-04-22  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/074.Swoot/074.Swoot  
tags: [ ios.org, OMV, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os hablo de la aplicación Swoot (red social de podcast?).  

+ [https://swoot.com/papafriki](https://swoot.com/papafriki)

<audio controls>
  <source src="https://archive.org/download/074.Swoot/074.Swoot.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
