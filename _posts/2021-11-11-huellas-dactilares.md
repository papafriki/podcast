---
layout: post  
title: "PPF-Huellas dactilares"  
date: 2021-11-11  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/207.-huellas-dactilares/207.HuellasDactilares  
tags: [Huellas dactilares, bidelto, adelto, Bertillon, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os traigo un monotemático sobre huellas dactilares.  



<audio controls>
  <source src="https://archive.org/download/207.-huellas-dactilares/207.HuellasDactilares.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
