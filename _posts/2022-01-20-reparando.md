---
layout: post  
title: "PPF-Reparando que es gerundio"  
date: 2022-01-20  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/217.-reparando-que-es-gerundio/217.ReparandoQueEsGerundio  
tags: [Geek Pako, Reparaciones, Móviles, One Plus, Pixel, DNIeRemote, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento que he reparado varios móviles y os hablo de la aplicación  DNIeRemote.  


Podcast de la semana: Orbita Friki -- The Expansive 6ª temporada

[https://www.ivoox.com/the-expanse-temporada-6-episodio-1-audios-mp3_rf_80251508_1.html](https://www.ivoox.com/the-expanse-temporada-6-episodio-1-audios-mp3_rf_80251508_1.html)


Aplicación DNIeRemote

[https://www.dnielectronico.es/portaldnie/PRF1_Cons02.action?pag=REF_1015&id_menu=68](https://www.dnielectronico.es/portaldnie/PRF1_Cons02.action?pag=REF_1015&id_menu=68)
<br>
<audio controls>
  <source src="https://archive.org/download/217.-reparando-que-es-gerundio/217.ReparandoQueEsGerundio.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>


Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/<br>
