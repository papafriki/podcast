---
layout: post  
title: "PPF-Skill de Alexa, Ivoox y anécdotas del DNI"  
date: 2020-10-29  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/150.-skill-alexa-ivoox-anecdotas-dni/150.SkillAlexaIvooxAnecdotasDNI  
tags: [Ivoox, Skill, Alexa, Amazon, Pablonidas, Anécdotas, DNI, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre la skill de Alexa que se ha currado Pablonidas, hablo de Ivoox Originals y os cuento tres anécdotas del DNI.  

<audio controls>
  <source src="https://archive.org/download/150.-skill-alexa-ivoox-anecdotas-dni/150.SkillAlexaIvooxAnecdotasDNI.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
