---
layout: post  
title: "Asociaciones"  
date: 2018-10-29  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/048.Asociaciones/048.Asociaciones  
tags: [ AMAPAMU, Asociación Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada, en el podcast de hoy os hablo de las charlas de la Asociación Madrileña de Partos Múltiples (AMAPAMU) y de la Asociación Podcast.  
 
En las charlas de AMAPAMU Pilar González nos recomendó leer a los niños los siguientes libros:

* El monstruo de los colores (Editorial Bruño).
* Cuentos cortos para dormir.
* Cuentos budistas.

Estos dos últimos fueron recomendados porque siempre llevan una moraleja asociada a cada cuento que nos permitirá poder comentar con los niños el cuento.

Referencias del episodio de hoy:

+ [AMAPAMU](http://http://www.amapamu.org//)
+ [Asociación Podcast](http://www.asociacionpodcast.es/)

<audio controls>
  <source src="https://archive.org/download/048.Asociaciones/048.Asociaciones.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
