---
layout: post  
title: "PPF-De vacaciones"  
date: 2020-08-04  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/138.-de-vacaciones/138.DeVacaciones  
tags: [  Vacaciones, Cacharreogeek, Playa, Piscina, Wallapop, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento como estamos llevando las vacaciones y la ultima compra hecha en wallapop.  


La red social del cacharreogeek

[hhttp://www.cacharreogeek.es/](http://www.cacharreogeek.es/)


<audio controls>
  <source src="https://archive.org/download/138.-de-vacaciones/138.DeVacaciones.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
