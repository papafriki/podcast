---
layout: post  
title: "Esperando noticias"  
date: 2019-10-17  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/098.esperando/098.Esperando  
tags: [Citrone, Smartwatch, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os comento que sigo esperando noticias del C4 que sigue en el taller, sobre la charla de smartwatch reciente, freebuds y algo más.



<audio controls>
  <source src="https://archive.org/download/098.esperando/098.Esperando.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/)
