---
layout: post  
title: "PPF-Reunión improvisada"  
date: 2021-12-03  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/210.-reunion-improvisada/210.Reunion_improvisada  
tags: [Kedada, Insanity Tech, Sospechosos Habituales, Papá Friki]  
comments: true 
---
Buenas muchachada hoy damos la bienvenida a Adriano del podcast insanity tech, comento la reunión con MazinguerAstur y otros compañeros de Cacharreo Geek y un par de temas más... pero es que no son horas de grabar.  

Podcast de la semana: Los Jozeles enlace al video
<br>
[https://www.youtube.com/watch?v=kG--7k21LY4https://www.youtube.com/watch?v=kG--7k21LY4](https://www.youtube.com/watch?v=kG--7k21LY4)



<audio controls>
  <source src="https://archive.org/download/210.-reunion-improvisada/210.Reunion_improvisada.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
