---
layout: post  
title: "Freedompop y Podcast a velocidad rápida"  
date: 2018-04-04  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/009.FreedompopPodcastRapidos/009.FreedompopPodcastRapidos    
tags: [feedburner,Samsung,GearS2,Podcast,Papa Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento mi experiencia con Freedompop y como escucho los podcast rápidos.  
![](https://papafriki.gitlab.io/podcast/images/logo.png)  


Referencias de hoy:  
+ [Freedompop](https://my.freedompop.com/)  
+ [@Monosdelespacio](https://twitter.com/monosdelespacio)  

<audio controls>
  <source src="https://archive.org/download/009.FreedompopPodcastRapidos/009.FreedompopPodcastRapidos.mp3" type="audio/mpeg">
</audio>


Y recuerda, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
