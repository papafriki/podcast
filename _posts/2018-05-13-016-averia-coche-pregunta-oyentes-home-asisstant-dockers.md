---
layout: post  
title: "Averia del coche, pregunta a los oyentes y raspberry pi"  
date: 2018-05-13  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/016.AveriaCochePreguntaOyentesHomeAsisstantDockers/016.AveriaCochePreguntaOyentesHomeAsisstantDockers  
tags: [Citroen, Averia, Raspberry, Dockers, Homeasisstant, Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada, en el episodio de hoy os pongo al día de como va la avería del coche familiar, os lanzo una pregunta a la audiencia, os comento que he conseguido mostrar el clima en home asisstant y añadir dockers a la raspberry pi.  

Os recuerdo la pregunta, ¿Sabéis como hay que dictarle a google por voz para que tenga en cuenta los signos de puntuación?

Asi luce la pestaña de **Home Assistant** mostrando el clima de mi ciudad
![](https://i.imgur.com/xj1nTdt.jpg)

Referencias de hoy:
+ [Podcast uGeek](https://ugeek.github.io/.docker-raspberry-linux/)  


<audio controls>
  <source src="https://archive.org/download/016.AveriaCochePreguntaOyentesHomeAsisstantDockers/016.AveriaCochePreguntaOyentesHomeAsisstantDockers.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
