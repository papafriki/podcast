---
layout: post  
title: "PPF-Cosas de casa. Resubido"  
date: 2021-08-19  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/195.-cosasdecasa/195.Cosasdecasa  
tags: [Ropa, Lavadoras, Lavavajillas, Cuchara, Cuchillo, Tenedor, Series, Papá Friki]  
comments: true 
---
Buenas muchachada hoy me acompaña Laura para sacar los trapos sucios de casa. Resubo el capitulo que me han chivado que el otro audio daba problemas para descargarse. 


Espero que lo disfrutéis, para nosotros ha sido una buena experiencia.  

 
<audio controls>
  <source src="https://archive.org/download/195.-cosasdecasa/195.Cosasdecasa.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
