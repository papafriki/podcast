---
layout: post  
title: "PPF-Spreaker, lavadora y TPV en el D.N.I."  
date: 2022-03-23  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/226.-lavadora-tpvdni/226.LavadoraTPVDni  
tags: [Spreaker, lavadora, Bosch, TPVs, Twitter, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre un problema con la plataforma de Spreaker, otra avería en la lavadora y sobre la implementación de TPVs en las oficinas de expdeción  de D.N.I. y T.I.E.  



Podcast de la semana: Orbita Laika el podcast - ¿Por qué dormimos? 
<br>
[https://www.ivoox.com/orbita-laika-el-podcast-capitulo-11-por-audios-mp3_rf_83798119_1.html](https://www.ivoox.com/orbita-laika-el-podcast-capitulo-11-por-audios-mp3_rf_83798119_1.html)



<br>
Primer episodio de Monos del Espacio:

[https://podcastaddict.com/episode/136955484](https://podcastaddict.com/episode/136955484)


<br>

<br>
Mi hilo sobre la contratación de los TPV en el D.N.I.:

[https://twitter.com/papa_friki/status/1506006436874420224](https://twitter.com/papa_friki/status/1506006436874420224)


<br>
<audio controls>
  <source src="https://archive.org/download/226.-lavadora-tpvdni/226.LavadoraTPVDni.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
