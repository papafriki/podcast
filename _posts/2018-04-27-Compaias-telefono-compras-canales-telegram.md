---
layout: post  
title: "Copmpañias de teléfono y compras en canales de Telegram"  
date: 2018-04-27  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/014.CompaiasTelefonoComprasCanalesTelegram/014.Compa%C3%B1iasTelefonoComprasCanalesTelegram  
tags: [Freedompop, Orange, Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os comento que he dado de baja la cuenta de Freedompop y que le saqué en la renovación a Orange.  

![](https://i.imgur.com/5bGMDxp.jpg)


<audio controls>
  <source src="https://archive.org/download/014.CompaiasTelefonoComprasCanalesTelegram/014.Compa%C3%B1iasTelefonoComprasCanalesTelegram.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
