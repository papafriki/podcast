---
layout: post  
title: "Nuevos proyectos, recomendaciones de podcast y algo mas"  
date: 2018-07-06  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/26.ProyectosNuevos/26.ProyectosNuevos  
tags: [ Recomendaciones, La Razón de la Voz, Diogenes Digital, Chiringuito, Comeme el podcast, Averia, Netflix, Papá Friki]  
comments: true 
---
Buenas muchachada, en el episodio de hoy os comento mis nuevos proyectos en la vida 1.0, os recomiendo algunos podcast y comento alguna cosilla mas de mi día a día.  


Referencias del episodio de hoy:   
+ [@Thor_4  de Thor4 Podcast](https://twitter.com/thor_4)
+ [La Razón de la Voz](http://larazondelavoz.gitlab.io/)
+ [Diogenes Digital](http://feedpress.me/DiogenesDigital)
+ [El chiringuito podcastaero](http://elchiringuitopodcastero.blogspot.com/)
+ [El parque de Bender](https://www.ivoox.com/podcast-parque-bender_sq_f1463448_1.html)
+ [Comemé el Podcast](https://twitter.com/comemeelpodcast?lang=es)


<audio controls>
  <source src="https://archive.org/download/26.ProyectosNuevos/26.ProyectosNuevos.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
