---
layout: post  
title: "¿Que portatil Compro?"  
date: 2018-03-22  
categories: podcast
image: images/logo.png  
podcast_link: https://archive.org/download/QuePortatilCompro/QuePortatilCompro
tags: [podcast,portatil,colaboración,Papa Friki]  
comments: true 
---
El pasado 1 de marzo participé en el podcast de Monos del Espacio que tituló ¿Qué portátil me compro?  

Como siempre fue un placer compartir micrófono con Jose (@monosdelespacio), Andres y Pedro (@mosqueteroweb).  

<audio controls>
  <source src="https://archive.org/download/QuePortatilCompro/QuePortatilCompro.mp3" type="audio/mpeg">
</audio>


![](https://papafriki.gitlab.io/podcast/images/logo.png)  

Y recuerda, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
