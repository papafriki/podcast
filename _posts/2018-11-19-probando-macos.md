---
layout: post  
title: "Probando MacOs"  
date: 2018-11-19  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/051.ProbandoMacOs/051.ProbandoMacOs  
tags: [ Cervezas y Directos, Macbook Pro, Mojave, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento mis primeras experiencias con MacOs Mojave tras mas de 20 años usando windows o linux.  

Os dejo el cartel de Cervezas y Directos de éste sábado 24 de noviembre de 2018 en Alcobendas (Madrid): 

+ [Diogenes Digital](http://www.ivoox.com/p_sq_f1339791_1.html)
+ [Invita la casa](http://www.ivoox.com/p_sq_f199207_1.html)
+ [No hay cine sin palomitas](http://www.ivoox.com/p_sq_f1178206_1.html)
+ [Histocast](http://www.ivoox.com/p_sq_f132047_1.html)

<audio controls>
  <source src="https://archive.org/download/051.ProbandoMacOs/051.ProbandoMacOs.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
