---
layout: post  
title: "PPF-Jaime Javier Aguilar Brites"  
date: 2021-04-20  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/177.-jorge-javier-aguilar-brites/177.JorgeJavierAguilarBrites  
tags: [Jaime Javier Aguilar Brites, Historias, Papá Friki]  
comments: true 
---
Buenas muchachada hoy episodio diferente contando la historia de Jorge Javier Aguilar Brites.  



<audio controls>
  <source src="https://archive.org/download/177.-jorge-javier-aguilar-brites/177.JorgeJavierAguilarBrites.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
