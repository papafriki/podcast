---
layout: post  
title: "Impresiones Maratonpod"  
date: 2019-11-07  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/101.impresionesmaratonpod/101.ImpresionesMaratonpod  
tags: [MaratonPod, Pixel XL, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os traigo impresiones tras la Maratonpod y os comento que me pasa con el Pixel XL.  


<audio controls>
  <source src="https://archive.org/download/101.impresionesmaratonpod/101.ImpresionesMaratonpod.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
