---
layout: post  
title: "PPF-El caso surrealista de Correos"  
date: 2021-02-19  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/168.-el-caso-surrealista-correos/168.ElCasoSurrealistaCorreos  
tags: [Correos, Aliexpress, TAOX, Papá Friki]  
comments: true 
---
Buenas muchachada si bien en el último capítulo pensamos que se había terminado el caso con correos, ha habido novedades que os cuento.  


El podcast de TAOX
[https://anchor.fm/s/497642bc/podcast/rss](https://anchor.fm/s/497642bc/podcast/rss)

<audio controls>
  <source src="https://archive.org/download/168.-el-caso-surrealista-correos/168.ElCasoSurrealistaCorreos.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
