---
layout: post  
title: "Feliz dia del padre y maratón podcast"  
date: 2019-03-19  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/069.DiadelPadre/069.DiadelPadre  
tags: [ Dia del Padre, Maratón, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy actualizo varios temas y os invito a sumaros al maratón de podcast que se le ha ocurrido a MazinguerAstur.  

Referencias del episodio de hoy:


+ [Maratón de podcast presenciales MazinguerAstur](http://www.ivoox.com/33472899)

<audio controls>
  <source src="https://archive.org/download/069.DiadelPadre/069.DiadelPadre.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
