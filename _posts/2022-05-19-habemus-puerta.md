---
layout: post  
title: "PPF-Habemus puerta"  
date: 2022-05-19   
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/234.-habemus-puerta/234.HabemusPuerta  
tags: [ Puerta, Ascensor, Garaje, Expdición D.N.I., Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento que ya tenemos puerta nueva.  

<br><br>
Podcast de la semana:  Charlando con... una creadora con lana
<br>
[https://anchor.fm/alberto5757/episodes/CC-Creadora-con-lana-e1i4ogp](https://anchor.fm/alberto5757/episodes/CC-Creadora-con-lana-e1i4ogp)



<br>
<audio controls>
  <source src="https://archive.org/download/234.-habemus-puerta/234.HabemusPuerta.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
