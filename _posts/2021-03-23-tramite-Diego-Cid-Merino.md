---
layout: post  
title: "PPF-Tr@amite con Diego Cid Merino"  
date: 2021-03-23  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/173.-tramite-con-diego-cid-merino/173.TramiteConDiegoCidMerino  
tags: [DNI 3.0, Certificado Electrónico, Diego Cid Merino, FNMT, CQe-Solutions, Papá Friki]  
comments: true 
---
Buenas muchachada hoy hablo de la apliación Tr@amite con su autor Diego Cid Merino.  

Aplicación Tr@amite de CQe-Solutions

https://play.google.com/store/apps/dev?id=5450188462309874352


Desahogo Geek de Alex Pro

https://anchor.fm/s/1218850/podcast/rss  


<audio controls>
  <source src="https://archive.org/download/173.-tramite-con-diego-cid-merino/173.TramiteConDiegoCidMerino.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
