---
layout: post  
title: "PPF-Kedada Cacharreo Geek, oculus y más sobre la casa"  
date: 2022-06-01   
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/236.-kedada-oculus-casa/236.KedadaOculusCasa  
tags: [ Ascensor, Garaje,  Kedada, Cacharreo Geek, Amigos, Oculus Quest 2, Atico, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre la kedada de Cacharreo Geek, sobre las Oculus Quest 2 y como va la casa nueva.  

<br><br>
Tweet #TipsConsejosDelDNI
<br>
[https://twitter.com/papa_friki/status/1528133757101912065](https://twitter.com/papa_friki/status/1528133757101912065)



<br>
<audio controls>
  <source src="https://archive.org/download/236.-kedada-oculus-casa/236.KedadaOculusCasa.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>
