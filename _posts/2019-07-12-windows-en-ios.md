---
layout: post  
title: "Windows en un macbook pro"  
date: 2019-07-12  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/085.windowsenios/085.WindowsEnIos  
tags: [ Windows, iOs, Frikismo Puro, Papá Friki]  
comments: true 
---
Buenas muchacha, hoy os cuento que me han tocado el sorteo de Frikismo Puro y como conseguir un pincho usb con windows persistente y usarlo en un macbook pro.  

El video que he seguido es este
[https://www.youtube.com/watch?v=g77iw6hl2GU&t=590s](https://www.youtube.com/watch?v=g77iw6hl2GU&t=590s)

El fork que muestra la subida con barra de progreso
[https://github.com/noabody/RcloneBrowser](https://github.com/noabody/RcloneBrowser)

Para descargar la imagen iso de  Windows seguir este artículo

[https://www.softzone.es/2018/05/28/descargar-iso-windows-10-directamente/](https://www.softzone.es/2018/05/28/descargar-iso-windows-10-directamente/) 

<audio controls>
  <source src="https://archive.org/download/085.windowsenios/085.WindowsEnIos.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: <https://swoot.com/papafriki](https://swoot.com/papafriki>
+ MaratonPod: <https://archive.org/details/www.maratonpod.es>
