---
layout: post  
title: "Variadito"  
date: 2019-09-03  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/092.variadito/092.Variadito  
tags: [Mala suerte, Liux,  ChromeOS, Tweet Marker, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os continuo contando sobre la mala suerte que me persigue estos días y algo tecnológico sobre linux, ChomeOS y Tweet Marker.  


<audio controls>
  <source src="https://archive.org/download/092.variadito/092.Variadito.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/)
