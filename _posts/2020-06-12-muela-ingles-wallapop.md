---
layout: post  
title: "PPF-Muela, inlgés, desescalada y Wallapop"  
date: 2020-06-12  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/132.-muela-ingles-wallapop/132.MuelaInglesWallapop  
tags: [  Muela, Inlgés, Desescalada, Wallapop, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento sobre mi muela, inglés, fases de la desescalada personales y más casos de wallapop.  

La pagina que os comento de Europol en que podéis ayudar contra el abuso infantil es la siguiente:

[https://www.europol.europa.eu/stopchildabuse](https://www.europol.europa.eu/stopchildabuse)


<audio controls>
  <source src="https://archive.org/download/132.-muela-ingles-wallapop/132.MuelaInglesWallapop.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
