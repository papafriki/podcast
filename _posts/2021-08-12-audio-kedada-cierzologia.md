---
layout: post  
title: "PPF-Audio charla kedada con Pedro Cierzología"  
date: 2021-08-12  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/194.-audio-kedada-cierzologia/194.AudioKedadaCierzologia  
tags: [Kedada, Cierzología, Oculus Quest 2, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comparto el audio de la kedada con Pedro de Cierzología.  



 He intentado mejorar el audio en audacity pero finalmente he usado el servicio de 
 
 [https://auphonic.com/Podcast](https://auphonic.com/Podcast)


 
<audio controls>
  <source src="https://archive.org/download/194.-audio-kedada-cierzologia/194.AudioKedadaCierzologia.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
