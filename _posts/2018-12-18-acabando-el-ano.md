---
layout: post  
title: "Acabando el año"  
date: 2018-12-18  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/056.AcabandoElAno/056.AcabandoElAno  
tags: [ Disperso,Macbook Pro, Redmi Note 5, Papá Friki]  
comments: true 
---
Buenas muchachada,hoy os comento que leo las notas de los podcast, que me ha llegado el Redmi Note 5 de 4/64 y alguna cosilla mas.  

+ [Disperso https://www.ivoox.com/p_sq_f1305139_1.html](https://www.ivoox.com/p_sq_f1305139_1.html)

<audio controls>
  <source src="https://archive.org/download/056.AcabandoElAno/056.AcabandoElAno.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>
