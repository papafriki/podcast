---
layout: post  
title: "PPF-Positivo"  
date: 2022-01-04  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/214.-positivo/214.Positivo  
tags: [Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento brevemente que hemos dado positivo Leire, Laura y yo.  

<br>
<audio controls>
  <source src="https://archive.org/download/214.-positivo/214.Positivo.mp3" type="audio/mpeg">
  </audio>
  <br>
  Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
  <br>
  https://feedpress.me/sospechososhabituales
  <br>
  Os recuerdo, los métodos de contacto son:
  <br>
  + Web: <http://www.papafriki.es><br>
  + Twitter: <https://twitter.com/papa_friki><br>
  + Correo: <papafrikipodcast@gmail.com><br>
  + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
  + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
  + Feed itunes: <https://itunes.apple.com/es/podcast/
