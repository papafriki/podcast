---
layout: post  
title: "PPF-Rosalina resposición"  
date: 2022-05-01  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/127.-rosalina/127.Rosalina  
tags: [ Rosalina, Covid-19, Abuela, Bisabuela, Mamá, Papá Friki]  
comments: true 
---
Buenas muchachada hoy como cada 1 de mayo os traigo nuevamente la historia de Rosalina.  


<audio controls>
  <source src="https://archive.org/download/127.-rosalina/127.Rosalina.mp3" type="audio/mpeg">
</audio>


Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>


Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>

