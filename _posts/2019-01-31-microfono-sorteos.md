---
layout: post  
title: "Micrófono y sorteos"  
date: 2019-01-31  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/062.MicrofonoSorteos/062.MicrofonoSorteos  
tags: [ Micrófono, Boya, Sorteos, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento que me he comprado el micrófono Boya y los sorteos que he ganado.  


<audio controls>
  <source src="https://archive.org/download/062.MicrofonoSorteos/062.MicrofonoSorteos.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>
