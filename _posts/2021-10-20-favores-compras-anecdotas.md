---
layout: post  
title: "PPF-Favores, compras y anécdotas"  
date: 2021-10-20  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/204.-favores-compras-anecdotas/204.FavoresComprasAnecdotas  
tags: [ Pintureta Geek, Anédoctas, D.N.I.,Lenovo, Portátil, Wallapop, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os hablo de favores, últimas compras y anécdotas del D.N.I.  


Podcast de la semana: El gato de Turin

[https://www.ivoox.com/142-8211-tu-a-texas-yo-a-audios-mp3_rf_76876920_1.html](https://www.ivoox.com/142-8211-tu-a-texas-yo-a-audios-mp3_rf_76876920_1.html)



<audio controls>
  <source src="https://archive.org/download/204.-favores-compras-anecdotas/204.FavoresComprasAnecdotas.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
