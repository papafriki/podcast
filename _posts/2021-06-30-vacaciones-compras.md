---
layout: post  
title: "PPF-Vacaciones y compras"  
date: 2021-06-30  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/187.-vacaciones-compras/187.VacacionesCompras  
tags: [Vacaciones, Marina Dor, Compras, Lenovo, ThinkCentre, M700, Amazon Echo dot, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento que tal han ido las vacaciones y que he comprado últimamente.  



<audio controls>
  <source src="https://archive.org/download/187.-vacaciones-compras/187.VacacionesCompras.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
