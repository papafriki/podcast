---
layout: post  
title: "PPF-Kedada Pedro de Cierzologia"  
date: 2021-08-11  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/193.-kedada-pedro-cierzologia/193.KedadaPedroCierzologia  
tags: [ Apple, Kedada, Oculus Quest 2, Facebook, Samsung, JVC, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento la kedada con Pedro de Cierzología y un par de cosillas sobre Apple.  



Podcast de la semana: Kedada con Papá Friki de Cierzología

[https://www.ivoox.com/kedada-papafriki-audios-mp3_rf_73822900_1.html](https://www.ivoox.com/kedada-papafriki-audios-mp3_rf_73822900_1.html)


El grupo de televisores de Felix que comento:
[https://t.me/smart_tv_ofertas](https://t.me/smart_tv_ofertas)


<audio controls>
  <source src="https://archive.org/download/193.-kedada-pedro-cierzologia/193.KedadaPedroCierzologia.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
