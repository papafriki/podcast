---
layout: post  
title: "PPF-Ruedas o puertas"  
date: 2022-08-21  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/246.-puertas-oruedas/246.PuertasORuedas  
tags: [ Premios, Asociación Podcast, Charlando con..., debate, niños, Ruedas, Puertas, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os traigo un apasionado debate sobre si hay mas ruedas o puertas en el mundo... debate que escuché en su dia en algún podcast y que salió en una sobremesa con la familia.  

<br><br>
Se está votando la primera fase de los premios de la asociación podcast hasta el 31 de Agosto, si eres socio o simpatizante y te acuerdas de Charlando con... como podcast revelación se agradece tu voto.

[https://asociacionpodcast.es/votacion-inicial-premios-asociacion-podcast-edicion-2022/](https://asociacionpodcast.es/votacion-inicial-premios-asociacion-podcast-edicion-2022/)
<br><br>
Podcast de la semana: Charlando con un operario de cadena de montaje
<br><br>
[https://anchor.fm/alberto5757/episodes/CC-Operario-de-cadena-de-montaje-e1m5ee2/a-a8bk9gh](https://anchor.fm/alberto5757/episodes/CC-Operario-de-cadena-de-montaje-e1m5ee2/a-a8bk9gh)
<br><br>


<br>
<audio controls>
  <source src="https://archive.org/download/246.-puertas-oruedas/246.PuertasORuedas.mp3" type="audio/mpeg">  
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>



