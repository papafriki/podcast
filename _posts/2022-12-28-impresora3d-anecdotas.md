---
layout: post  
title: "PPF-Impresora 3D y anécdotas"  
date: 2022-12-28  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/267.-impresora-3-danecdotas/267.Impresora3DAnecdotas  
tags: [Creality, CR-6 SE, Impresora, 3D, Primeros Pasos, PLA, Anécdotas, D.N.I., Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento nuestra primera impresión en 3D y un par de anécdotas del D.N.I.  


<br><br>
Charlando con... un trabajador de Michelin
[https://spotifyanchor-web.app.link/e/k9j9bD5m1ub](https://spotifyanchor-web.app.link/e/k9j9bD5m1ub)
<br>

<br>
<audio controls>
  <source src="https://archive.org/download/267.-impresora-3-danecdotas/267.Impresora3DAnecdotas.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>


