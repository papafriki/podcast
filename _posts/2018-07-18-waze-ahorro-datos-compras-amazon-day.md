---
layout: post  
title: "Waze, ahorro de datos y compras Amazon Prime Day"  
date: 2018-07-18  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/028.WazeAhorroDatosComprasAmazonDay/028.WazeAhorroDatosComprasAmazonDay  
tags: [ Waze, eDucando Geek, Marcogeek, Amazon, Condenados Podcas, Androytecno,Papá Friki]  
comments: true 
---
Buenas muchachada, os comento las pruebas que he estado haciendo con Waze y el aviso de radares, las cosas que hago para ahorrar datos en verano y las compras que he hecho éste Amanzon Prime Day.  

![](https://i.imgur.com/SfVZmBu.jpg)

Referencias del episodio de hoy:   
+ [Teoría de los 6 grados](https://es.wikipedia.org/wiki/Seis_grados_de_separaci%C3%B3n)
+ [eDucando Geek](https://educandogeek.github.io/)
+ [Marcogeek](https://www.ivoox.com/podcast-marcogeek_sq_f1151977_1.html)
+ [Condenados Podcast](http://www.condenadospodcast.com/)
+ [Androytecno](http://androytecno.com/)
+ [t.me/PodcastAndroytecno](t.me/PodcastAndroytecno)


<audio controls>
  <source src="https://archive.org/download/028.WazeAhorroDatosComprasAmazonDay/028.WazeAhorroDatosComprasAmazonDay.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
