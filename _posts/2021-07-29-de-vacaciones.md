---
layout: post  
title: "PPF-De Vacaciones"  
date: 2021-07-29  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/191.-de-vacaciones/191.DeVacaciones  
tags: [Movistar, Problemas, D.N.I., Anécdotas, Series, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre unas compras a ciegas y la última película hemos ido a ver al cine.  


Podcast de la semana: Galego Geek Diario

[https://feeds.feedburner.com/GalegogeekDiario](https://feeds.feedburner.com/GalegogeekDiario)

<audio controls>
  <source src="https://archive.org/download/191.-de-vacaciones/191.DeVacaciones.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
