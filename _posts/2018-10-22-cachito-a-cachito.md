---
layout: post  
title: "Cachito a cachito"  
date: 2018-10-26  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/047.PodcastTroceado/047.PodcastTroceado  
tags: [ Lg Urbane, Compras, Papá Friki]  
comments: true 
---
Buenas muchachada, he cambiado el formato y el episodio de hoy se compone de varios mini-audios de diferentes momentos de la semana. Os cuento cosas que he ido descubriendo sobre el LG Urbane Watch y como fue la compra para Mazinguer, Faisco y Pablonidas.
Siento mucho la calidad del audio, pero se ha grabado en diferentes sitios, en el coche me recogió el audio por el micrófono del coche en lugar del móvil y para colmo Audacity sigue siendo un gran desconocido para mi.  

<audio controls>
  <source src="https://archive.org/download/047.PodcastTroceado/047.PodcastTroceado.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
