---
layout: post  
title: "PPF-Netflix turco, B.O.E. y problemas con tecnología"  
date: 2021-04-29  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/178.-netflix-turco/178.NetflixTurco  
tags: [ Netflix, Turquia, B.O.E., Tecnología, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento sobre el Netflix turco, preambulos en el B.O.E. y problemas con la tecnología.  


Página para comprar tarjetas de regalo:

[https://www.g2a.com/search?query=netflix](https://www.g2a.com/search?query=netflix)


Hola VPN

[https://play.google.com/store/apps/details?id=org.hola&hl=es&gl=US](https://play.google.com/store/apps/details?id=org.hola&hl=es&gl=US)


<audio controls>
  <source src="https://archive.org/download/178.-netflix-turco/178.NetflixTurco.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
