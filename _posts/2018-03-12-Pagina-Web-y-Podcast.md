---
layout: post  
title: "Página web y podcast"  
date: 2018-03-12  
categories: podcast
image: images/logo.png  
podcast_link: https://archive.org/download/002.HacerPaginayPodcast/002.HacerPaginayPodcast
tags: [blog, gitlab, Papa Friki]  
comments: true 
---
En el episodio de hoy trato de explicar como he hecho la página web que acompaña al podcast y con los nervios me he dejado sin decir los métodos de contacto... asi que en el siguiente espero no se me olviden.  
El curso sobre podcasting que comento en el episodio lo podeis seguir [aqui][curso].   

Un saludo
[curso]: https://www.youtube.com/watch?v=a6dCJpjLH3Q&t=222s
<audio controls>
  <source src="https://archive.org/download/002.HacerPaginayPodcast/002.HacerPaginayPodcast.ogg" type="audio/ogg">
  <source src="https://archive.org/download/002.HacerPaginayPodcast/002.HacerPaginayPodcast.mp3" type="audio/mpeg">
</audio>


![](https://papafriki.gitlab.io/podcast/images/logo.png)  

Y recuerda, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>

