---
layout: post  
title: "PPF-Jefa de negociado"  
date: 2021-12-14  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/212.-jefa-negociado/212.JefaNegociado  
tags: [Equipo D.N.I., Jefa negociado, D.N.I., Entrevista, Papá Friki]  
comments: true 
---
Buenas muchachada hoy me acompaña Inés, jefa de negociado de una oficina de expedición del D.N.I.  
<br>
<audio controls>
  <source src="https://archive.org/download/212.-jefa-negociado/212.JefaNegociado.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es><br>
+ Twitter: <https://twitter.com/papa_friki><br>
+ Correo: <papafrikipodcast@gmail.com><br>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en><br>
