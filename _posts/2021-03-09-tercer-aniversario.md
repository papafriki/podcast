---
layout: post  
title: "PPF-Tercer aniversario"  
date: 2021-03-09  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/171.-tercer-aniversario/171.TercerAniversario  
tags: [ Aniversario, Repaso, Sumando Podcast, Sospechosos Habituales, Papá Friki]  
comments: true 
---
Buenas muchachada hoy repasamos los tres años que llevo de podcast.  




<audio controls>
  <source src="https://archive.org/download/171.-tercer-aniversario/171.TercerAniversario.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
