---
layout: post  
title: "Agradecimientos, directos y compras"  
date: 2018-11-30  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/AgradecimientosDirectosCompras_201811/AgradecimientosDirectosCompras  
tags: [ Agradecimientos, Compras, Directos, Papá Friki]  
comments: true 
---

Buenas muchachada, lo primero es daros las gracias por los dibujitos y los audios que nos habéis hecho llegar para Leire, os comento también la experiencia de grabar el directo y os dejo enlace al directo de Dióegnes Digital en el que participé y por último os comento mis compras este Black Friday  


+ [Diogenes Digital su directo](https://pca.st/68gv)
+ [Sumando Podcast](https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml)


<audio controls>
  <source src="https://archive.org/download/AgradecimientosDirectosCompras_201811/AgradecimientosDirectosCompras.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>