---
layout: post  
title: "PPF-Variadito"  
date: 2021-05-26  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/183.-variadito/183.Variadito  
tags: [Alergia, Simyo, Tarifas, D.N.I., Anécdotas, Educación, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os hago un variadito rápido con alergias, colaboraciones en otros podcast y la mala educación de la gente.  


Vermut con la Asociación Podcast
[https://asociacionpodcast.es/2021/05/25/llega-el-vermut-de-la-asociacion/](https://asociacionpodcast.es/2021/05/25/llega-el-vermut-de-la-asociacion/)

<audio controls>
  <source src="https://archive.org/download/183.-variadito/183.Variadito.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
