---
layout: post  
title: "PPF-Aniversario, timos en la luz y pasaportes"  
date: 2022-03-10  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/224.-aniversario-luz-pasaportes/224.AniversarioLuzPasaportes  
tags: [Anirsario, podcast, factura, luz, timo, Energy, pasaporte, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os hablo sobre el cuarto aniversario del podcast, de la contratación que han aceptado en la luz mis padres y sobre el poder del pasaporte español.  



Podcast de la semana: Charlando con.. un tramitador procesal y administrativo
<br>
[https://anchor.fm/alberto5757/episodes/CC-Tramitador-procesal-y-administrativo-e1f9ce8](https://anchor.fm/alberto5757/episodes/CC-Tramitador-procesal-y-administrativo-e1f9ce8)


<br>
Página con el listado de pasaportes:

[https://www.henleyglobal.com/passport-index/compare](https://www.henleyglobal.com/passport-index/compare )


<br>
<audio controls>
  <source src="https://archive.org/download/224.-aniversario-luz-pasaportes/224.AniversarioLuzPasaportes.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
