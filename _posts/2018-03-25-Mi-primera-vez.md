---
layout: post  
title: "Mi primera vez..."  
date: 2018-03-25  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/006.MiPrimeraVez/006.MiPrimeraVez  
tags: [Uber,Cabify,Tesla,Podcast,Papa Friki]  
comments: true 
---
Buenas muchachada, os voy a contar mi primera vez... No seáis mal pensados, que es mi primera vez con Uber, Cabify y un Tesla.  

Os dejo un par de fotos del interior del Tesla, así como el [video con el atropello](https://twitter.com/TempePolice/status/976585098542833664) por parte del Uber. No os preocupéis que no se ve nada pues el vídeo se para justo antes ocurrir el trágico hecho.

![](https://papafriki.gitlab.io/podcast/images/uber_1.jpg)  
![](https://papafriki.gitlab.io/podcast/images/uber_2.jpg) 


<audio controls>
  <source src="https://archive.org/download/006.MiPrimeraVez/006.MiPrimeraVez.mp3" type="audio/mpeg">
</audio>


Y recuerda, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
