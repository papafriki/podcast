---
layout: post  
title: "Deporte dominguero"  
date: 2019-08-18  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/090.deportedominguero/090.DeporteDominguero  
tags: [Deporte, Freebuds, Almacenamiento único, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy entre jadeos os cuento que he vuelto a hacer deporte y que ya no sufro tantos microcortes en los freebuds y como lo he solucionado, también paso de puntillas y os comento sobre el "almacenamiento único".  


<audio controls>
  <source src="https://archive.org/download/090.deportedominguero/090.DeporteDominguero.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/)
