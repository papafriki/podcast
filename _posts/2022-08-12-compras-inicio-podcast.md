---
layout: post  
title: "PPF-Compras y como empiezo con los podcast"  
date: 2022-08-12  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/246.-compras-inicio-podcast/246.ComprasInicioPodcast  
tags: [Western Digital, NAS, Fallo,  Podcast, Podcasting, Podcasters, Compras, Silla, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento las ultimas compras estando de vacaciones, os explico como empecé a escuchar podcast y comento algo mas de como va la recuperación del western digital.  



<br><br>
Finalmente no hay enlace al directo que hizo @monosdelespacio ya que por diversos problemas técnicos no quedó grabado.

Podcast de la semana: Charlando con un operario de cadena de montaje
<br><br>
[https://anchor.fm/alberto5757/episodes/CC-Operario-de-cadena-de-montaje-e1m5ee2/a-a8bk9gh](https://anchor.fm/alberto5757/episodes/CC-Operario-de-cadena-de-montaje-e1m5ee2/a-a8bk9gh)
<br><br>


<br>
<audio controls>
  <source src="https://archive.org/download/246.-compras-inicio-podcast/246.ComprasInicioPodcast.mp3" type="audio/mpeg">  
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>

