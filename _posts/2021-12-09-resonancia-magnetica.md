---
layout: post  
title: "PPF-Resonancia magnética"  
date: 2021-12-09  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/211.-resosnancia-magnetica/211.ResosnanciaMagnetica  
tags: [Resonancia, Magnética, Constitución, Carreras populares, Wii, Arcade, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento sobre la resonancia magnética que me han hecho.  

Podcast de la semana: Radio ambulante -- El sabor de las palabras
<br>
[https://www.ivoox.com/sabor-palabras-audios-mp3_rf_78949997_1.html](https://www.ivoox.com/sabor-palabras-audios-mp3_rf_78949997_1.html)



<audio controls>
  <source src="https://archive.org/download/211.-resosnancia-magnetica/211.ResosnanciaMagnetica.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
