---
layout: post  
title: "Más civsimo y solución al problema en  php"  
date: 2020-02-25  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/118.mascivismosolucionphp/118.MasCivismoSolucionPHP  
tags: [Civismo, PHP, The Geek Cow, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento la falta de civismo vista en la confirmación de Nuria y la solución al problema del php de la mano de Rapejim y comento un traspiés con la raspberry.  



<audio controls>
  <source src="https://archive.org/download/118.mascivismosolucionphp/118.MasCivismoSolucionPHP.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)
