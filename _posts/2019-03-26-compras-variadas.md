---
layout: post  
title: "Compras variadas"  
date: 2019-03-26  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/070.ComprasVariadas/070.ComprasVariadas  
tags: [ Descargas Mente, Wallapop, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento las últimas compras (no todas son tecnológicas) y que he desvirtualizado a otro gran podcaster.  


<audio controls>
  <source src="https://archive.org/download/070.ComprasVariadas/070.ComprasVariadas.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
