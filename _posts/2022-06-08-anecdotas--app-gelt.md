---
layout: post  
title: "PPF-Anécdotas y aplicación Gelt"  
date: 2022-06-08  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/237.-anecdotas-gelt/237.AnecdotasGelt  
tags: [Anécdotas, D.N.I., Gelt, Ciudadano electrónico, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os va cargado de anécdotas del D.N.I. y de una aplicación de cashback llamada Gelt.  



Podcast de la semana: Charlando con un subdirector de un centro penitenciario
<br><br>
[https://anchor.fm/alberto5757/episodes/CC-Subdirector-de-un-centro-penitenciario-e1jhcvp](https://anchor.fm/alberto5757/episodes/CC-Subdirector-de-un-centro-penitenciario-e1jhcvp)
<br><br>
Podcast de la semana: Ciudadano electrónico -- Personas diversas, distintas y discriminadas por las Administraciones
<br><br>
[https://www.ivoox.com/episodio-42-personas-diversas-distintas-discriminadas-audios-mp3_rf_87317408_1.html](https://www.ivoox.com/episodio-42-personas-diversas-distintas-discriminadas-audios-mp3_rf_87317408_1.html)
<br><br>
Aplicación Gelt. Mi código de referido SUHIUEN o bien con el enlace de abajo

[https://geltapp.onelink.me/3887771487?pid=User_invite&af_web_dp=https%3A%2F%2Fgelt.com%2Fcountry-redirect&c=4521576](https://geltapp.onelink.me/3887771487?pid=User_invite&af_web_dp=https%3A%2F%2Fgelt.com%2Fcountry-redirect&c=4521576)


<br>
<audio controls>
  <source src="https://archive.org/download/237.-anecdotas-gelt/237.AnecdotasGelt.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
