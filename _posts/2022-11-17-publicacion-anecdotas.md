---
layout: post  
title: "PPF-Publicación y anécdotas"  
date: 2022-11-17  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/260.-publicacion-anecdotas/260.PublicacionAnecdotas  
tags: [ Archive.org, Gitlab.io, Anécdotas, D.N.I., Publicación, Papá Friki]  
comments: true 
---
Buenas muchachada hoy explico porqué mis audios aparecen los primeros del día en vuestros programas de reproducción y más anécdotas del D.N.I.  



<br><br>
Charlando con... un actor de teatro
<br><br>
[https://spotifyanchor-web.app.link/e/k9j9bD5m1ub](https://spotifyanchor-web.app.link/e/k9j9bD5m1ub)
<br>
<br><br>
Sumando -- Autosumando de vuelta
<br><br>
[https://go.ivoox.com/rf/96439144](https://go.ivoox.com/rf/96439144)

<br>
<audio controls>
  <source src="https://archive.org/download/260.-publicacion-anecdotas/260.PublicacionAnecdotas.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
