---
layout: post  
title: "PPF-Whatsapp, despistado y apliciación tv"  
date: 2020-05-23  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/129.-whatsapp-despiste/129.WhatsappDespiste  
tags: [ Whatsapp, Despistado, Photocall.tv, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento un problema grabe con whastapp, que vivo en un despiste generalizado y que he encontrado una buena aplicación para ver la tv en cualquier dispositivo.  

![](https://i.imgur.com/oFP0mij.jpg)

<audio controls>
  <source src="https://archive.org/download/129.-whatsapp-despiste/129.WhatsappDespiste.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/](https://www.maratonpod.es/)
+ Fedd Maratonpod: [http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml](http://www.ivoox.com/maratonpod_fg_f1775991_filtro_1.xml)