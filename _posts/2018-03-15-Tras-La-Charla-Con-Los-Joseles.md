---
layout: post  
title: "Tras la charla con los Joseles"  
date: 2018-03-15  
categories: podcast
image: images/logo.png  
podcast_link: https://archive.org/download/03.TrasLaCharlaConLosJoseles/03.TrasLaCharlaConLosJoseles
tags: [podcast, Los Joseles, Papa Friki]  
comments: true 
---
Tras participar ayer en el podcast de [Los Joseles](https://www.losjoseles.com/) con GeekJoze; Jose de monos del espacio, Maria e Iñaki hago hoy una pequeña reflexión y comento algunos datos más.  
Las estadísticas que se comentan las podeis consultar en los siguientes enlaces:

 
[Victimas violencia de género](http://www.poderjudicial.es/cgpj/es/Temas/Estadistica-Judicial/Estadistica-por-temas/Datos-penales--civiles-y-laborales/Violencia-domestica-y-Violencia-de-genero/Victimas-mortales-de-violencia-de-genero-y-violencia-domestica-en-ambito-de-la-pareja-o-ex-pareja/)   


[Victimas de tráfico](http://www.dgt.es/es/seguridad-vial/estadisticas-e-indicadores/accidentes-30dias/series-historicas/)  


Un saludo


<audio controls>
  <source src="https://archive.org/download/03.TrasLaCharlaConLosJoseles/03.TrasLaCharlaConLosJoseles.ogg" type="audio/ogg">
  <source src="https://archive.org/download/03.TrasLaCharlaConLosJoseles/03.TrasLaCharlaConLosJoseles.mp3" type="audio/mpeg">
</audio>


![](https://papafriki.gitlab.io/podcast/images/logo.png)  

Y recuerda, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
