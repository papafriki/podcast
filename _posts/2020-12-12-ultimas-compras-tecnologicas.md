---
layout: post  
title: "PPF-Últimas compras tecnológicas "  
date: 2020-12-12  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/157.-ultimas-compras/157.UltimasCompras  
tags: [ Aliexpress, Amazon, One Plus 6T, HD, Sata, IDE, Stadia, Google, Kindle, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento las últimas compras tecnológicas que he realizado.  



<audio controls>
  <source src="https://archive.org/download/157.-ultimas-compras/157.UltimasCompras.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)