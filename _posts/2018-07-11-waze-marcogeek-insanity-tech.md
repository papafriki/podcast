---
layout: post  
title: "Waze, Marcogeek e Insanity Tech"  
date: 2018-07-11  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/027.WazeMarcoGeekInsanityTech/027.WazeMarcoGeekInsanityTech  
tags: [ Recomendaciones, La Razón de la Voz,Waze, Marcogeek, Insanity Tech, Papá Friki]  
comments: true 
---
Buenas muchachada, en el episodio de hoy os comento mi uso con Waze y que he descubierto nuevos podcast.  


Referencias del episodio de hoy:   
+ [Waze: 21 trucos para exprimir a fondo este navegador](https://www.xataka.com/aplicaciones/waze-19-trucos-para-exprimir-a-fondo-esta-app-de-trafico-y-navegacion)
+ [La Razón de la Voz](http://larazondelavoz.gitlab.io/)
+ [Marcogeek](https://www.ivoox.com/podcast-marcogeek_sq_f1151977_1.html)
+ [Insanity Tech](https://twitter.com/insanitytechpod)


<audio controls>
  <source src="https://archive.org/download/027.WazeMarcoGeekInsanityTech/027.WazeMarcoGeekInsanityTech.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>