---
layout: post  
title: "¿Que portatil Compro? Segunda parte"  
date: 2018-03-23  
categories: podcast
image: images/logo.png  
podcast_link: https://archive.org/download/QuePortatilCompro2/QuePortatilCompro2
tags: [podcast,portatil,colaboración,Papa Friki]  
comments: true 
---
El pasado 18 de marzo participé nuevamente en el podcast de Monos del Espacio que tituló "Recomendaciones concretas y ejemplos de portátiles".

Nuevamente compartir micrófono con Jose (@monosdelespacio), Andrés y Pedro (@mosqueteroweb) ha sido una gran experiencia y os animo a que veáis [el video subido a youtube](https://www.youtube.com/watch?v=5a_fh89bwko) donde podréis ver varios ejemplos concretos y aparatos nada corrientes que tienen Andres y Pedro.  


<audio controls>
  <source src="https://archive.org/download/QuePortatilCompro2/QuePortatilCompro2.mp3" type="audio/mpeg">
</audio>


![](https://papafriki.gitlab.io/podcast/images/portatil.png)  

Y recuerda, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
