---
layout: post  
title: "PPF-Habemus nevera"  
date: 2022-06-26  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/239.-habemus-nevera./239.HabemusNevera  
tags: [Ascensor, Anécdotas, D.N.I., Reclamaciones, Aire acondicionada, Gelt, Calor, Papá Friki]  
comments: true 
---
Buenas muchachada hoy es episodio rápido para contaros que ya tenemos nevera.  



Podcast de la semana: Charlando con un subdirector de un centro penitenciario
<br><br>
[https://anchor.fm/alberto5757/episodes/CC-Subdirector-de-un-centro-penitenciario-e1jhcvp](https://anchor.fm/alberto5757/episodes/CC-Subdirector-de-un-centro-penitenciario-e1jhcvp)
<br><br>

Aplicación Gelt. Mi código de referido SUHIUEN o bien con el enlace de abajo

[https://geltapp.onelink.me/3887771487?pid=User_invite&af_web_dp=https%3A%2F%2Fgelt.com%2Fcountry-redirect&c=4521576](https://geltapp.onelink.me/3887771487?pid=User_invite&af_web_dp=https%3A%2F%2Fgelt.com%2Fcountry-redirect&c=4521576)


<br>
<audio controls>
  <source src="https://archive.org/download/239.-habemus-nevera./239.HabemusNevera.mp3" type="audio/mpeg">  
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>
