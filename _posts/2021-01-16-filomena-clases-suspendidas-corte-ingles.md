---
layout: post  
title: "PPF-Filomena, clases interrumpidas y malas experiencias con el Corte Inglés"  
date: 2021-01-16  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/162.-filomena-clases-corte-ingles/162.FilomenaClasesCorteIngles  
tags: [ Filomena, El Corte Inglés, Concesionario, Chino, Nuc, Plex, Jellyfin, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento como hemos vivido el paso de Filomena por  nuestra casa, la interrupción de las clases durante semana y media y las malas experiencias con el corte ingles y su gestión de pedidos.  


<audio controls>
  <source src="https://archive.org/download/162.-filomena-clases-corte-ingles/162.FilomenaClasesCorteIngles.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
