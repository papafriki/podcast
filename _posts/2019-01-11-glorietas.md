---
layout: post  
title: "Glorietas y anécdotas de tráfico"  
date: 2019-01-11  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/059.Glorietas/059.Glorietas  
tags: [ Tráfico, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento cosas sobre las glorietas y anécdotas de tráfico.  


<audio controls>
  <source src="https://archive.org/download/059.Glorietas/059.Glorietas.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>
