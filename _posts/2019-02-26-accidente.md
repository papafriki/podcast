---
layout: post  
title: "Accidente"  
date: 2019-02-26  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/065.Accidente/065.Accidente  
tags: [ Accidente, Anécdotas, Papá Friki]  
comments: true 
---
Buenas muchachada, os cuento el porqué no he grabado estas últimas semanas y he estado tan desaparecido en redes sociales y grupos de telegram.  

<audio controls>
  <source src="https://archive.org/download/065.Accidente/065.Accidente.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>
