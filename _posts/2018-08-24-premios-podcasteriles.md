---
layout: post  
title: "Premios Podcasteriles"  
date: 2018-08-24  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/PremiosPodcasteriles/PremiosPodcasteriles  
tags: [ premios, Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada  hoy entrego mis premios "podcasteriles". Entender que son basados en los podcast que conozco y escucho, seguramente haya muchos mas y muy buenos que se merecen premio. Haz una lista y entrega tus premios y compártelos con los demás.  


Referencias del episodio de hoy:

+ [MarcoGeek](https://www.ivoox.com/podcast-marcogeek_sq_f1151977_1.html)
+ [MazinguerAstur Podcast](http://feeds.feedburner.com/Mazingerastur)
+ [A ratos podcast](http://www.ivoox.com/p_sq_f1463037_1.html)
+ [Podcast Linux](http://www.ivoox.com/p_sq_f1297890_1.html)
+ [Chiringuito digital](http://www.ivoox.com/p_sq_f197142_1.html)
+ [Comercial Geek](http://www.ivoox.com/p_sq_f143401_1.html)
+ [Los Joseles](http://www.ivoox.com/p_sq_f1280363_1.html)
+ [Monos del espacio](http://www.ivoox.com/p_sq_f1148042_1.html)
+ [Lo que tu digas](http://www.ivoox.com/p_sq_f1424550_1.html)
+ [Sospechosos habituales](http://www.ivoox.com/p_sq_f1564393_1.html)


<audio controls>
  <source src="https://archive.org/download/PremiosPodcasteriles/PremiosPodcasteriles.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
