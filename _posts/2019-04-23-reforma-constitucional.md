---
layout: post  
title: "Reforma constitucional"  
date: 2019-04-23  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/075.ReformaConstitucional/075.ReformaConstitucional  
tags: [ Reforma Constitucional, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy me salgo de la temática habitual y os comento como se hace una reforma constitucional.  

Os sigo dejando aqui mi perfil en la red swoot
+ [https://swoot.com/papafriki](https://swoot.com/papafriki)

<audio controls>
  <source src="hhttps://archive.org/download/075.ReformaConstitucional/075.ReformaConstitucional.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>