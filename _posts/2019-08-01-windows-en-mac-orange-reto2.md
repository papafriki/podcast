---
layout: post  
title: "Mas windows en Macbook, orange y reto"  
date: 2019-08-01  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/088.windowsenmacorangereto2/088.WindowsenMacOrangeReto2  
tags: [Macbook, Windows, Orange, Reto, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os comento mas pruebas en el MacBook Pro usando windows, los problemas en casa con Orange y nomino a dos podcasters más (@Thor_4 y @Pablonidas).  


<audio controls>
  <source src="https://archive.org/download/088.windowsenmacorangereto2/088.WindowsenMacOrangeReto2.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/)