---
layout: post  
title: "Jugando con los mellizos"  
date: 2018-05-16  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/017.JugandoConLosMellizos/017.JugandoConLosMellizos  
tags: [Mellizos, Familia, Juego, Podcast, Papá Friki]  
comments: true 
---
Buenas muchachada, en el episodio de hoy os comparto un audio familiar en el que me podréis escuchar jugando en el sofá de casa junto a los mellizos.  

<audio controls>
  <source src="https://archive.org/download/017.JugandoConLosMellizos/017.JugandoConLosMellizos.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
