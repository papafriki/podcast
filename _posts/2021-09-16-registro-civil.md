---
layout: post  
title: "PPF-Registro Civil"  
date: 2021-09-16  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/199.-registro-civil/199.RegistroCivil  
tags: [iPad, Audio momentos, Soledad, Telegram, Registro Civil, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os intento explicar los cambios que vendran en el Registro civil.  

Podcast de la semana: Audio momentos - La soledad en tiempos de internet (con Ricardo Espinosa)

[https://www.ivoox.com/ep-149-la-soledad-tiempos-internet-audios-mp3_rf_75098283_1.html](https://www.ivoox.com/ep-149-la-soledad-tiempos-internet-audios-mp3_rf_75098283_1.html)


<audio controls>
  <source src="https://archive.org/download/199.-registro-civil/199.RegistroCivil.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
