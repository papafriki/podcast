---
layout: post  
title: "Un día con Papá Friki"  
date: 2018-04-12  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/011UnDiaConPapaFriki/011undiaconpapafriki  
tags: [Linux,Xiaomi,Compras,Podcast,Papa Friki]  
comments: true 
---
Buenas muchachada, hoy he querido compartir con vosotros un día que ha empezado con emociones fuertes. Ha continuado con la primera vez que dono sangre y comentaros la experiencia de compra en la web de Hipercor y pequeños problemillas con el feed de  iTunes.  

![](https://i.imgur.com/ptIkjYd.png)  

Os dejo enlace del podcast que me puse a escuchar en directo [Los Joseles](https://www.losjoseles.com/Podcast/) y de [EducandoGeek](https://educandogeek.github.io/) de @jgurillo :   


<audio controls>
  <source src="https://archive.org/download/011UnDiaConPapaFriki/011undiaconpapafriki.mp3" type="audio/mpeg">
</audio>

Y recuerda, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
