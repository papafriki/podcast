---
layout: post  
title: "PPF-Tiddlywiki y recomendaciones D.N.I. (Resubido)"  
date: 2022-11-24  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/261.-tiddly-wiky/261.TiddlyWiky  
tags: [ TiddlyWiki, Wiki, Programas, Ultimos Feedlipinas, D.N.I.Papá Friki]  
comments: true 
---
Buenas muchachada hoy os hablo sobre TiddlyWiki y recomendaciones a la hora de hacerse el D.N.I.  


Dejo reseña de un buen video de youtube para ver mas explicaciones y ejemplos de TiddlyWiki creado por @FrancisMeetze
<br><br>
[https://www.youtube.com/watch?v=ZMGpAW0z_Bo&list=PLzZCajspPU_UjFn0uy-J9URz0LP4zhxRK](https://www.youtube.com/watch?v=ZMGpAW0z_Bo&list=PLzZCajspPU_UjFn0uy-J9URz0LP4zhxRK)
<br><br>
Charlando con... un actor de teatro
<br><br>
[https://spotifyanchor-web.app.link/e/k9j9bD5m1ub](https://spotifyanchor-web.app.link/e/k9j9bD5m1ub)
<br>
<br><br>
Sumando -- Autosumando de vuelta
<br><br>
[https://go.ivoox.com/rf/96439144](https://go.ivoox.com/rf/96439144)

<br>
<audio controls>
  <source src="https://archive.org/download/261.-tiddly-wiky/261.TiddlyWiky.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>


