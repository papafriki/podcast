---
layout: post  
title: "PPF-PPPoe el protocolo del infierno"  
date: 2020-12-08  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/156.-pppoe-orbi/156.PPPoeOrbi  
tags: [ PPPoe, Protocolo, Digi, Router, ZTE, ZXHNH298A, Fikidelto, Orbi Papá Friki]  
comments: true 
---
Buenas muchachada hoy cuento como voy perdiendo la batalla contra DIGI y el protocolo PPPoe.  


Artículo de Fikidelto.com con la forma de obtener las credenciales en un router marca ZTE modelo ZXHNH298A de la compañía DiGi

[https://www.frikidelto.com/tutorial/como-conseguir-el-usuario-y-contrasena-pppoe-para-instalar-un-router-neutro/](https://www.frikidelto.com/tutorial/como-conseguir-el-usuario-y-contrasena-pppoe-para-instalar-un-router-neutro/)


<audio controls>
  <source src="https://archive.org/download/156.-pppoe-orbi/156.PPPoeOrbi.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
