---
layout: post  
title: "PPF-Tragsatec y Netflix"  
date: 2022-12-23  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/266.-tragsatec-netflix/266.TragsatecNetflix  
tags: [Tragsatec, Loteria, Navidad, Netflix, Turquia, SMS, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento que tocó la lotería en Tragsatec y como solucioné la recarga del Netflix turco.  



Programas para conseguir sms en Turquia u otros paises.
<br><br>
[https://5sim.biz/](https://5sim.biz/)
<br><br>
Pingme
[https://play.google.com/store/apps/details?id=tel.pingme&hl=es&gl=US&pli=1](https://play.google.com/store/apps/details?id=tel.pingme&hl=es&gl=US&pli=1)
<br><br>
Charlando con... un trabajador de Michelin
[https://spotifyanchor-web.app.link/e/k9j9bD5m1ub](https://spotifyanchor-web.app.link/e/k9j9bD5m1ub)
<br>

<br>
<audio controls>
  <source src="https://archive.org/download/266.-tragsatec-netflix/266.TragsatecNetflix.mp3" type="audio/mpeg">
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
