---
layout: post  
title: "PPF-Medicos, padres responsables, Orbi y el NUC"  
date: 2020-12-02  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/155.-medicos-orbi/155.MedicosOrbi  
tags: [ Médicos, Resposabilidad, NUC, ORBI, Linux Mint, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os comento cosas de medicos y padres responsables, sobre el ORBI y el NUC.  


<audio controls>
  <source src="https://archive.org/download/155.-medicos-orbi/155.MedicosOrbi.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
