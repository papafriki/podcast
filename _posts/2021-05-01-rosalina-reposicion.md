---
layout: post  
title: "PPF-Rosalina resposición"  
date: 2021-05-01  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/179.-rosalina-resposicion/179.RosalinaResposicion  
tags: [ Rosalina, Covid-19, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os traigo nuevamente la historia de Rosalina.  


<audio controls>
  <source src="https://archive.org/download/179.-rosalina-resposicion/179.RosalinaResposicion.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
