---
layout: post  
title: "PPF-Mejorando audio y variado"  
date: 2021-11-17  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/208.-mejorando-sonidoy-variado/208.MejorandoSonidoyVariado  
tags: [ Asociación Podcast, Mahjong en 10 minutos, Audacity, Reaper, Podcast, Series, Ciudadano Electrónico, Charlas con Aita, JPOD, Papá Friki]  
comments: true 
---
Buenas muchachada hoy  trato de mejorar el sonido, os cuento series que he visto y un variado que espero os guste.  


El directorio de la Asociación Podcast

[https://asociacionpodcast.es/directorio/](https://asociacionpodcast.es/directorio/)

Si te registras y usas el código AG2Q02 tendrás 5€ de descuento el primer año.

[https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02](https://www.asociacionpodcast.es/registrarse/socio/?coupon=AG2Q02)


<audio controls>
  <source src="https://archive.org/download/208.-mejorando-sonidoy-variado/208.MejorandoSonidoyVariado.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
