---
layout: post  
title: "Que mala suerte!"  
date: 2019-08-27  
categories: podcast  
image: https://i.imgur.com/nrxE1AM.png  
podcast_link: https://archive.org/download/091.quemalasuerte/091.QueMalaSuerte  
tags: [Mala suerte, Despistes, Papá Friki]  
comments: true 
---
Buenas muchachada, hoy os cuento las cosas que me han pasado estas vacaciones que se pueden considerar mala suerte o despistes.  


<audio controls>
  <source src="https://archive.org/download/091.quemalasuerte/091.QueMalaSuerte.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
+ MaratonPod: [https://www.maratonpod.es/)