---
layout: post  
title: "Temas atrasados"  
date: 2018-12-07  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/055.TemasAtrasados/055.TemasAtrasados  
tags: [ LG Watch Urbane, Mac, Papá Friki]  
comments: true 
---

Buenas muchachada, Buenas muchachada, hoy os comento temas que me he ido dejando en el tintero.  


+ [Bot elige tu propia aventura @tupropiaaventura_bot](https://t.me/tupropiaaventura_bot)

<audio controls>
  <source src="https://archive.org/download/055.TemasAtrasados/055.TemasAtrasados.mp3" type="audio/mpeg">
</audio>

Os recuerdo, los métodos de contacto son:

+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ Web: <https://papafriki.gitlab.io/podcast>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Sumando Podcast: <https://www.ivoox.com/sumando-podcast_fg_f1636623_filtro_1.xml>
