---
layout: post  
title: "PPF-Aerotermia y D.N.I."  
date: 2022-07-24  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link:   https://archive.org/download/243.-aerotermia-dni/243.AerotermiaDNI  
tags: [Aerotermia, Casa, D.N.I., Familias Numerosas, Papá Friki]  
comments: true 
---
Buenas muchachada os cuento algo mas sobre la aerotermia y una curiosidad de las familias numerosas.  

<br><br>
Podcast de la semana: Charlando con un pintor de coches
<br><br>
[https://anchor.fm/alberto5757/episodes/CC-Pintor-de-coches-e1ks058](https://anchor.fm/alberto5757/episodes/CC-Pintor-de-coches-e1ks058)
<br><br>


<br>
<audio controls>
  <source src="https://archive.org/download/243.-aerotermia-dni/243.AerotermiaDNI.mp3" type="audio/mpeg">  
</audio>
<br><br>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:
<br>
https://feedpress.me/sospechososhabituales
<br>

<br>
Os recuerdo, los métodos de contacto son:
<br>
 + Web: <http://www.papafriki.es><br>
 + Twitter: <https://twitter.com/papa_friki><br>
 + Correo: <papafrikipodcast@gmail.com><br>
 + YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw><br>
 + Feed Podcast: <http://feeds.feedburner.com/papafriki><br>
 + Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
 + linktr.ee: <https://linktr.ee/papa_friki>

