---
layout: post  
title: "PPF-FileBot y problemas con OpenMediaVault"  
date: 2020-10-15  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/148.-file-bot-omv/148.FileBotOMV  
tags: [ProximaStream, FileBot, OMV, Windows, Portable, Linux, Papá Friki]  
comments: true 
---
Buenas muchachada hoy le damos la bienvenida a ProximaStream a la red de Sospechoso Habituales, os cuento el programa FileBot que he descuerto recientemente y unos problemillas que he tenido con un servidor OMV.  

FileBot
[https://github.com/barry-allen07/FB-Mod/releases](https://github.com/barry-allen07/FB-Mod/releases)
<audio controls>
  <source src="https://archive.org/download/148.-file-bot-omv/148.FileBotOMV.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
+ Perfil swoot: [https://swoot.com/papafriki](https://swoot.com/papafriki)
