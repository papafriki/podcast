---
layout: post  
title: "PPF-Medicos y mas carpeta de podcast"  
date: 2021-11-04  
categories: podcast  
image: https://papafriki.gitlab.io/podcast/images/logo.png  
podcast_link: https://archive.org/download/206.-medicos-carpeta-pocast/206.MedicosCarpetaPodcast  
tags: [Médicos, Carpeta, Podcast, Lenovo, Planeta Cuñao, Iniciativa Espielberg, Papá Friki]  
comments: true 
---
Buenas muchachada hoy os cuento que estamos pachuchos por casa y explico más sobre la carpeta de podcast.  


Podcast de la semana: Planeta Cuñao Los vampiros.


[https://www.ivoox.com/vampiros-audios-mp3_rf_77544358_1.html](https://www.ivoox.com/vampiros-audios-mp3_rf_77544358_1.html)


<audio controls>
  <source src="https://archive.org/download/206.-medicos-carpeta-pocast/206.MedicosCarpetaPodcast.mp3" type="audio/mpeg">
</audio>

Podcast asociado a la red de SOSPECHOSOS HABITUALES. Suscríbete con este feed:

https://feedpress.me/sospechososhabituales

Os recuerdo, los métodos de contacto son:

+ Web: <http://www.papafriki.es>
+ Twitter: <https://twitter.com/papa_friki>
+ Correo: <papafrikipodcast@gmail.com>
+ YouTube: <https://www.youtube.com/channel/UCAl-ql8V1IUZKYYLhhUVCYw>
+ Feed Podcast: <http://feeds.feedburner.com/papafriki>
+ Feed itunes: <https://itunes.apple.com/es/podcast/pap%C3%A1-friki/id1371105069?l=en>
